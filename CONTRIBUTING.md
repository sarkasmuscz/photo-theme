Many of rules are covered by ESLint.

Others are here:

1. In every block (class, function, cond, loop...) use this style of brackets:

```js
if (cond) {
    // to something here.

}
```

3. If there are more then 3 close-brackets, use style like this:

```js
for (...) {

    for (...) {
    
        for (...) {
            // Some code here.
            if (...) {
                // last block - will have blank line below
                
            }
        }
    }
}
```

2. If you comment in nested block, use newline, like here:

```js
for (let i in t) {
    
    // Nested loop
    for (let n in i) {
        
        // Nested if
        if (cond) // ...
    
    }

}
```

3. Everytime use `let` keyword instead of const or var