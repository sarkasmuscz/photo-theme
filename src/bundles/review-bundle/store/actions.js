// @flow

export default {
  archive ({dispatch, rootGetters}): void {
    let selectedPhotos: Array<number> = rootGetters.selectedPhotos
    dispatch('updateSelectedPhotoMetadata', {
      index: selectedPhotos[0] - 1,
      change: 'phase',
      value: 'gallery'
    }, {root: true})

  },

  edit (): void {
    // Todo: implement.
  },

  resubmitPhotos ({commit, rootState}): void {
    for (let selectedPhoto in rootState.selectedPhotos) {
      if (Object.prototype.hasOwnProperty.call(rootState.selectedPhotos, selectedPhoto)) {
        selectedPhoto = rootState.selectedPhotos[selectedPhoto]

        if (rootState.photos[selectedPhoto - 1].phase === 'review') {
          commit('UPDATE_PHOTO', {
            'index': selectedPhoto - 1,
            'change': 'phase',
            'value': 'gallery'
          }, {root: true})

          let selectedStocks = rootState.photos[selectedPhoto - 1].stocks
          let selectedStocksArray = []
          selectedStocks = Object.keys(selectedStocks).map((key, index) => {
            if (selectedStocks[key].status === 'rejected') {
              let object = {
                code: selectedStocks[key].code,
                name: selectedStocks[key].name,
                status: 'pending'
              }

              selectedStocksArray.push(object)
            }

            delete selectedStocks[key]
          })

          commit('UPDATE_PHOTO', {
            'index': selectedPhoto - 1,
            'change': 'stocks',
            'value': selectedStocksArray
          }, {root: true})
        }

      }
    }
  }
}
