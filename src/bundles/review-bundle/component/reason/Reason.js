// @flow

import swal from 'sweetalert'

/**
 * Component shown in right panel which loads information about rejection reason and allows to
 * do action with rejected photo - edit, resubmit and archive.
 *
 * @tutorial reviewBundle
 *
 * @module Bundles/ReviewBundle/Component/Reason
 * */
export default {
  name: 'Reason',
  props: {
    photos: {
      type: Array,
      required: true
    },

    selectedPhotos: {
      type: Array,
      required: true
    }
  },
  computed: {
    headline (): string {
      return this.photos[this.selectedPhotos[0] - 1].reasonOfRejection.headline
    },

    description (): string {
      return this.photos[this.selectedPhotos[0] - 1].reasonOfRejection.description
    },

    single (): boolean {
      return this.selectedPhotos.length === 1
    },

    notUndefined (): ?boolean {
      if (this.single) {
        return this.photos[this.selectedPhotos[0] - 1] !== undefined
          && this.photos[this.selectedPhotos[0] - 1].reasonOfRejection !== undefined

      }

      return null
    }
  },
  methods: {
    /**
     * Resubmits photo to all stock, where it is rejected.
     * */
    resubmit (): void {
      swal({
        title: 'Resubmit photo?',
        text: 'Do you really wanna resubmit this photo as it is? Probably it will be rejected this time too.',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('reviewModule/resubmitPhotos')
        }
      })
    },

    edit (): void {
      this.$router.push('/review-edit/' + this.selectedPhotos[0])
    },

    archive (): void {
      this.$store.dispatch('reviewModule/archive')
    }
  }
}

