import ContextMenu from 'vue-context-menu'
import GalleryPhoto from '../../../gallery-bundle/component/gallery-photo/GalleryPhoto.vue'
import CrxRightWrapper from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'

/**
 * View similar to [gallery view] but it shows just rejected photos. In this view you can work with these photos.
 * @module Review
 * */
export default {
  name: 'Review',
  components: {
    GalleryPhoto,
    CrxRightWrapper,
    ContextMenu
  },
  created () {
    this.$store.dispatch('deselectAllPhotos')
  },
  computed: {
    /**
     * All rejected photos.
     * @property {Array} photos
     * */
    photos () {
      return this.$store.getters.photos('review').filter((photo) => {
        return photo !== undefined && photo.deleted !== undefined && !photo.deleted
      })
    }
  },
  methods: {
    /**
     * Selects photo/photos. Event handler for GalleryPhoto panels.
     * @param {Array} selected - List of photos to select.
     * */
    galleryPhotoSelect (selected) {
      this.$store.dispatch('selectPhotos', {
        galleryName: 'gallery',
        photos: selected
      })
    }
  }
}
