import VueSelect from 'vue-select'
import Vue2Dropzone from 'vue2-dropzone'

/**
 * This component is first part of the application which user will see. It allows him to import photos, RAWs,
 * releases and pair them with data from stocks.
 *
 * @tutorial initBundle
 *
 * @module Bundles/InitBundle/View/InitView
 * */
export default {
  name: 'InitView',
  components: {
    VueSelect,
    Vue2Dropzone
  },
  /**
   * Data.
   * */
  data () {
    return {
      names: true,
      addRaws: false,
      rawNames: false,
      assignReleases: false,
      releaseNames: false,
      stocks: [],
      initDataData: {}
    }
  },
  /**
   * Initialize few settings of Dropzone.
   * */
  mounted () {
    this.$refs.photos.dropzone.on('addedfile', (file) => {
      let reader = new FileReader()
      reader.onload = (event) => {
        this.resizedataURL(event.target.result, 350, 350, file)
      }
      reader.readAsDataURL(file)
    })

    this.$refs.photos.dropzone.on('processing', () => {
      this.$refs.photos.dropzone.options.autoProcessQueue = true
    })

    this.$refs.photos.dropzone.on('sending', (file, xhr, formData) => {
      formData.append('stockImages', JSON.stringify(file.stockImages || {}))
    })
  },
  methods: {
    /**
     * Gets initData (whole data from stocks) from browser extension and after complete calls another step.
     * */
    initData (callback) {
      let stocks = []

      for (let stock in this.stocks) {
        if (Object.prototype.hasOwnProperty.call(this.stocks, stock)) {
          stocks.push(this.stocks[stock].code)
        }
      }

      chrome.runtime.sendMessage(
        'abmgjhflkmhpoiaibbjlhhdckeonliee',
        {action: 'init', stock: stocks[0]},
        (response) => {
          this.initDataData[stocks[0]] = {photos: response}
          callback()
        }
      )
    },

    /**
     * Starts initializing user data, which means start pairing of submitted photos to photos on stock.
     * */
    start () {
      this.initData(() => {
        let files = this.$refs.photos.dropzone.files
        let stocks = this.stocks
        let initData = this.initDataData
        let restOfImages = JSON.parse(JSON.stringify(this.initDataData))

        let map = files.map((file, fileId) => {
          return {
            photo: file, fileId: fileId, stockImages: stocks.map((stock) => {
              return {
                stock: stock.code, images: initData[stock.code].photos.filter((photo, key) => {
                  if (photo.original_filename === file.name) {
                    delete restOfImages[stock.code].photos[key]

                    return true
                  }

                  return false
                })
              }
            })
          }
        })

        this.check(map, restOfImages)
      })
    },

    /**
     * Checks if every submitted photo have an mate on every stock.
     * */
    check (map, restOfImages) {
      let returnMap = map
      let counter = 0

      returnMap.map((pair, pairKey) => {
        pair.stockImages.map((match) => {
          // If there are two images with same filename, then check them.
          if (match.images.length > 1) {
            let highestMismatch = 100
            match.images.map((image) => {
              this.toDataURL(image.thumbnail, (data) => {
                resemble(data)
                  .compareTo(pair.photo.dataToCompare)
                  .scaleToSameSize()
                  .ignoreAntialiasing()
                  .onComplete((dataMismatch) => {
                    counter++

                    if (highestMismatch > dataMismatch.misMatchPercentage) {
                      highestMismatch = dataMismatch.misMatchPercentage
                      returnMap[pairKey].checkedStockImages = {}
                      returnMap[pairKey].checkedStockImages[match.stock] = image
                    }

                    if (counter === returnMap.length) {
                      this.submitMap(returnMap)
                      this.$refs.photos.dropzone.processQueue()
                    }
                  })
              })

              return null
            })
          } else if (match.images.length === 1) {
            match.images.map((image) => {
              returnMap[pairKey].checkedStockImages = {}
              returnMap[pairKey].checkedStockImages[match.stock] = image

              return null
            })

            counter++

            if (counter === returnMap.length) {
              this.submitMap(returnMap)
              this.$refs.photos.dropzone.processQueue()
            }
          } else {
            let highestMismatch = 100
            Object.keys(restOfImages).map((stock) => {
              restOfImages[stock].photos.map((image) => {
                this.toDataURL(image.thumbnail, (data) => {
                  resemble(data)
                    .compareTo(pair.photo.dataToCompare)
                    .scaleToSameSize()
                    .ignoreAntialiasing()
                    .onComplete((dataMismatch) => {
                      counter++

                      if (highestMismatch > dataMismatch.misMatchPercentage) {
                        highestMismatch = dataMismatch.misMatchPercentage

                        if (typeof returnMap[pairKey].checkedStockImages === 'undefined') {
                          returnMap[pairKey].checkedStockImages = {}
                        }

                        returnMap[pairKey].checkedStockImages[match.stock] = image
                      }

                      if (counter === returnMap.length) {
                        this.submitMap(returnMap)
                        this.$refs.photos.dropzone.processQueue()
                      }
                    })
                })

                return null
              })

              return null
            })
          }

          return null
        })

        return null
      })
    },

    /**
     * Assigns map data into files ready to submit.
     * */
    submitMap (map) {
      map.map((pair) => {
        this.$refs.photos.dropzone.files[pair.fileId].stockImages = pair.checkedStockImages

        return null
      })
    },

    /**
     * Little helper. Converts image url into base64.
     * */
    toDataURL (url, callback) {
      let xhr = new XMLHttpRequest()
      xhr.onload = function () {
        let reader = new FileReader()
        reader.onloadend = function () {
          callback(reader.result)
        }
        reader.readAsDataURL(xhr.response)
      }
      xhr.open('GET', url)
      xhr.responseType = 'blob'
      xhr.send()
    },

    /**
     * Resize base64 to required size.
     *
     * @see https://stackoverflow.com/questions/20958078/resize-a-base-64-image-in-javascript-without-using-canvas
     * (2nd answer)
     * */
    resizedataURL (datas, wantedWidth, wantedHeight, file) {
      // We create an image to receive the Data URI
      let img = document.createElement('img')

      // When the event "onload" is triggered we can resize the image.
      img.onload = function () {
        // We create a canvas and get its context.
        let canvas = document.createElement('canvas')
        let ctx = canvas.getContext('2d')

        // We set the dimensions at the wanted size.
        canvas.width = wantedWidth
        canvas.height = wantedHeight

        // We resize the image with the canvas method drawImage();
        ctx.drawImage(this, 0, 0, wantedWidth, wantedHeight)

        file.dataToCompare = canvas.toDataURL()
      }

      // We put the Data URI in the image's src attribute
      img.src = datas
    }
  }
}
