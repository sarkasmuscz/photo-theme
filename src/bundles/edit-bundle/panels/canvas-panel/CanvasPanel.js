import editor from '../../../editor-bundle/editor'

/**
 * This component renders currently edited photo. It will use canvas in the future.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Panels/CanvasPanel
 * */
export default {
  name: 'CanvasPanel',
  props: {
    photo: {
      type: Object,
      required: false
    },

    showOriginal: {
      type: Boolean
    }
  },
  /**
   * On the start of component, apply adjustments.
   * */
  afterMount () {
    this.update()
  },
  methods: {
    /**
     * Update canvas when photo (snapshot) or showOriginal changed.
     * */
    update () {
      const canvas = this.$refs['my-canvas']
      const context = canvas.getContext('2d')

      context.clearRect(0, 0, canvas.width, canvas.height)

      let photo = this.photo

      if (this.photo !== undefined) {

        // Load an image
        let imageObj = new Image()
        let showOriginal = this.showOriginal
        imageObj.onload = function () {
          canvas.width = imageObj.width
          canvas.height = imageObj.height
          context.drawImage(this, 0, 0)

          let imgData = context.getImageData(0, 0, canvas.width, canvas.height)

          // If showOriginal is true, don't apply adjustments (renders unedited photo).
          if (!showOriginal) {
            imgData = editor.edit(imgData, photo.snapshot)
          }

          context.putImageData(imgData, 0, 0)
        }

        imageObj.src = this.photo.imageLinkFull
      }
    }
  },
  watch: {
    /**
     * Watching photo for a snapshot change.
     * */
    photo: {
      /**
       * Handler.
       * */
      handler () {
        this.update()
      },
      deep: true
    },

    /**
     * If showOriginal is true, don't apply adjustments on photo.
     * */
    showOriginal () {
      this.update()
    }
  }
}
