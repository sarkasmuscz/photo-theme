// @flow

import GalleryPhoto from '../../../gallery-bundle/component/gallery-photo/GalleryPhoto.vue'

/**
 * Contains photos in edit phase and allow them to be selected to edit.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Panels/PhotoFilmPanel
 * */
export default {
  name: 'PhotoFilmPanel',
  components: {
    GalleryPhoto
  },
  props: {
    photos: {
      type: Array,
      required: true
    }
  },
  methods: {
    /**
     * Selects photo/photos. Event handler for GalleryPhoto panels.
     * @param {Array} selected - List of photos to select.
     * */
    galleryPhotoSelect (selected: Array<number>): void {
      this.$store.dispatch('selectPhotos', {
        galleryName: 'gallery',
        photos: selected
      })
    }
  }
}
