// @flow

import CrxRightWrapper from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'
import PhotoFilmPanel from '../../panels/photo-film-panel/PhotoFilmPanel.vue'
import CanvasPanel from '../../panels/canvas-panel/CanvasPanel.vue'

/**
 * View of edit page. It is used to many cases of edit: single-edit, review-edit, version-edit.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/View/Edit
 * */
export default {
  name: 'Edit',
  components: {
    CrxRightWrapper,
    PhotoFilmPanel,
    CanvasPanel
  },
  /**
   * Updated.
   */
  updated () : void {
    this.update()
  },

  /**
   * Created.
   */
  created () : void {
    this.$store.dispatch('editModule/resetShowOriginal')
    this.update()

    if (this.$route.name === 'edit') {
      this.$store.dispatch('deselectAllPhotos')
    }
  },

  methods: {
    /**
     * Selects currently edited photo, when on single-edit page.
     */
    update () : void {
      if (this.$route.name === 'single-edit' || this.$route.name === 'review-edit') {
        this.$store.dispatch('selectPhotos', {
          photos: [this.$route.params.id]
        })
      }
    }
  }
}
