/**
 * Getters.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Store/Actions
 * */
export default {
  /**
   * Returns showOriginal option.
   * */
  showOriginal (state) {
    return state.showOriginal
  }
}
