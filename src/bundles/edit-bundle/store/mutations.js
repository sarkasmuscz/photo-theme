/**
 * Mutations.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Store/Actions
 * */
export default {
  /**
   * Updates showOriginal option.
   * */
  SHOW_ORIGINAL (state, showOriginal) {
    state.showOriginal = showOriginal
  }
}
