import actions from './actions'
import getters from './getters'
import mutations from './mutations'

/**
 * Store module for operations applied on edited photos - mainly managing their snapshots.
 * This module doesn't implement own getters and mutations - it commits mutations from gallery package.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Store/EditBundle
 * */
export default {
  namespaced: true,

  actions: actions,
  getters: getters,
  mutations: mutations,
  state: {
    /**
     * This options is used to compare original and edited versions of photo.
     * If true, CanvasPanel shows unedited photo.
     * */
    showOriginal: false
  }
}
