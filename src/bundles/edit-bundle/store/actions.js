// @flow

import type { SnapshotType } from '../../../types/SnapshotType.flow'

/**
 * Actions.
 *
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Store/Actions
 * */
export default {
  // Todo: implement edit history.
  /**
   * Updates edit snapshot - edit values.
   */
  updateSnapshot ({commit, rootState}: Object, snapshot: SnapshotType) {
    if (rootState.selectedPhotos.length === 1) {
      commit('UPDATE_PHOTO', {
        index: rootState.selectedPhotos[0] - 1,
        change: 'snapshot',
        value: snapshot
      }, {root: true})

    }
  },

  /**
   * Updates edit snapshot of the edited version - edit values.
   */
  updateVersionSnapshot ({commit, rootState}: Object,
                         data: { snapshot: SnapshotType, photoId: number, versionId: number }) {
    let versions = JSON.parse(JSON.stringify(rootState.photos[data.photoId - 1].versions))
    versions[data.versionId - 1].snapshot = data.snapshot

    commit('UPDATE_PHOTO', {
      index: data.photoId - 1,
      change: 'versions',
      value: versions
    }, {root: true})
  },

  /**
   * Sends selected photos to metadata page.
   */
  sendToMetadata ({commit, rootState}: Object) {
    if (rootState.selectedPhotos.length === 1) {
      commit('UPDATE_PHOTO', {
        index: rootState.selectedPhotos[0] - 1,
        change: 'phase',
        value: 'metadata'
      }, {root: true})

    }
  },

  /**
   * Switches showOriginal option which is used to compare original and edited version of photo.
   * */
  showOriginal ({commit, state}: Object) {
    commit('SHOW_ORIGINAL', !state.showOriginal)
  },

  /**
   * Resets showOriginal to default false.
   * */
  resetShowOriginal ({commit}: Object) {
    commit('SHOW_ORIGINAL', false)
  }
}
