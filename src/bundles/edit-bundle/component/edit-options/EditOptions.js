// @flow

import RangeSlider from 'vue-range-slider'
// Import types.
import type { SnapshotType } from '../../../../types/SnapshotType.flow'

/**
 * Component which is rendered in right panel (crx-right-wrapper). It applies editing on currently selected photo.
 * EditOptions cares about photo's snapshot, which represents editing values like saturation, exposure etc.
 *
 * @tutorial snapshot
 * @tutorial editBundle
 *
 * @module Bundles/EditBundle/Component/EditOptions
 * */
export default {
  name: 'EditOptions',
  components: {
    RangeSlider
  },
  props: {
    /**
     * Actual snapshot of the edited photo.
     * @type {SnapshotType}
     * */
    snapshotStore: {
      type: Object,
      required: false,
      default: () => {
        return {
          temperature: 0,
          tint: 0,
          exposure: 0,
          contrast: 0,
          highlights: 0,
          shadows: 0,
          blacks: 0,
          whites: 0,
          clarity: 0,
          dehaze: 0,
          vibrance: 0,
          saturation: 1,
          sharpening: {
            level: 0,
            radius: 0,
            detail: 0
          },
          noiseReduction: {
            level: 0,
            detail: 0
          },
          lensCorrection: false
        }
      }
    }
  },
  /**
   * Copy snapshot.
   * @constructor
   * */
  created (): void {
    this.snapshot = JSON.parse(JSON.stringify(this.snapshotStore))
  },
  /** Snapshot local copy. This is needed due to data flow pattern. */
  data (): { snapshot: SnapshotType } {
    return {
      snapshot: {
        temperature: 0,
        tint: 0,
        exposure: 0,
        contrast: 0,
        highlights: 0,
        shadows: 0,
        blacks: 0,
        whites: 0,
        clarity: 0,
        dehaze: 0,
        vibrance: 0,
        saturation: 1,
        sharpening: {
          level: 0,
          radius: 0,
          detail: 0
        },
        noiseReduction: {
          level: 0,
          detail: 0
        },
        lensCorrection: false
      }
    }
  },
  methods: {
    /**
     * Updates snapshot in the store.
     */
    updateSnapshot (): void {
      if (this.$route.name === 'version-edit') {
        this.$store.dispatch('editModule/updateVersionSnapshot', {
          snapshot: this.snapshot,
          photoId: this.$route.params.photoId,
          versionId: this.$route.params.versionId
        })

      } else {
        this.$store.dispatch('editModule/updateSnapshot', this.snapshot)

      }
    },

    /**
     * Sends photo from edit phase into metadata.
     *
     * @tutorial phases
     * */
    sendToMetadata () : void {
      this.$store.dispatch('editModule/sendToMetadata')
    },

    /**
     * Resets all options to default.
     * */
    reset (): void {
      this.snapshot = {
        temperature: 0,
        tint: 0,
        exposure: 0,
        contrast: 0,
        highlights: 0,
        shadows: 0,
        blacks: 0,
        whites: 0,
        clarity: 0,
        dehaze: 0,
        vibrance: 0,
        saturation: 1,
        sharpening: {
          level: 0,
          radius: 0,
          detail: 0
        },
        noiseReduction: {
          level: 0,
          detail: 0
        },
        lensCorrection: false
      }

      this.updateSnapshot()
    },

    /**
     * Reverts snapshot to lastVersion state.
     * */
    revert (): void {
      this.snapshot = this.$store.getters.photos('all')[parseInt(this.$route.params.id) - 1].lastVersion.snapshot

      this.updateSnapshot()
    },

    /**
     * Shows original version of edited photo, without adjustments.
     * */
    showOriginal (): void {
      this.$store.dispatch('editModule/showOriginal')
    },

    /**
     * Resubmits edited photo on review.
     * */
    resubmit (): void {
      this.$store.dispatch('selectPhotos', {
        photos: [parseInt(this.$route.params.id)]
      })

      this.$store.dispatch('reviewModule/resubmitPhotos')

      this.$router.push('/gallery')
    }
  },
  watch: {
    snapshotStore: {
      /**
       * Watch original snapshot and update if changed.
       * */
      handler (newValue: SnapshotType): void {
        this.snapshot = JSON.parse(JSON.stringify(newValue))
      },

      deep: true
    }
  }
}
