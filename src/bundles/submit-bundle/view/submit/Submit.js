// @flow

import CrxRightWrapper from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'
import SortPhotosMixin from '../../../gallery-bundle/mixins/sortPhotosMixin'
import GalleryPhoto from '../../../gallery-bundle/component/gallery-photo/GalleryPhoto.vue'
import ContextMenu from 'vue-context-menu'
import swal from 'sweetalert'
// Import types.
import type { PhotoType } from '../../../../types/PhotoType.flow'

/**
 * View gallery of the photo ready to submit (updated or new photos).
 * @module Gallery
 * */
export default {
  name: 'Submit',
  mixins: [
    SortPhotosMixin
  ],
  components: {
    CrxRightWrapper,
    GalleryPhoto,
    ContextMenu
  },
  /**
   * Deselects photos to avoid unexpected operations with photos selected from other pages.
   * */
  created () {
    this.$store.dispatch('deselectAllPhotos')
  },
  computed: {
    /**
     * All photos.
     * @property {Array} photos
     * */
    photos (): Array<PhotoType> {
      return this.$store.getters.photos('submit').filter((photo) => {
        return photo !== undefined && photo.deleted !== undefined && !photo.deleted
      })
    }
  },
  methods: {
    /**
     * Selects photo/photos. Event handler for GalleryPhoto panels.
     * @param {Array} selected - List of photos to select.
     * */
    galleryPhotoSelect (selected: Array<number>): void {
      this.$store.dispatch('selectPhotos', {
        galleryName: 'gallery',
        photos: selected
      })
    },

    /**
     * Returns selected photos back to metadata phase.
     * */
    sendToMetadata (): void {
      this.$store.dispatch('updateSelectedPhotoMetadata', {
        change: 'phase',
        value: 'metadata'
      })
    },

    /**
     * Removes selected photos from evidence. ♻
     * */
    removePhoto (): void {
      swal({
        title: 'Remove selected photos?',
        text: 'Do you really wanna remove all currently selected photos?',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('removeSelectedPhotos')

        }
      })
    },

    downloadLatest (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}`
    },

    downloadRaw (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}/raw`
    }
  }
}
