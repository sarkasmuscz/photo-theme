// @flow

import type PhotoType from '../../../types/PhotoType.flow.js'

/**
 * Photo pre-submit validator for Shutterstock.
 *
 * @module Bundles/SubmitBundle/Validators/ShutterstockValidator
 * */
export default class ShutterstockValidator {
  /**
   * Validates photo by metadata.
   *
   * In case of Shutterstock, there has to be at least 7 keywords and at maximum 50.
   * Caption length is not specified on SS explicitly. Also 1 category selected required.
   * */
  validate (photo: PhotoType) {
    try {
      return Object.keys(photo.categories.shutterstock).length >= 1
        && photo.caption.length > 5
        && photo.keywords.length >= 7
        && photo.keywords.length <= 50
    } catch (err) {
      return false
    }
  }
}
