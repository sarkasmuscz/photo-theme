// @flow

import type PhotoType from '../../../types/PhotoType.flow.js'

/**
 * Photo pre-submit validator for Dreamstime.
 *
 * @module Bundles/SubmitBundle/Validators/DreamstimeValidator
 * */
export default class DreamstimeValidator {
  /**
   * Validates photo by metadata.
   *
   * In case of Dreamstime, you need to specify 3 categories, headline with 5 words and caption with 10 words.
   * Range of keywords are 7 - 80. Minimum of keywords are taken from Shutterstock as on Dreamstime it is not explicitly
   * specified.
   * */
  validate (photo: PhotoType) {
    try {
      return Object.keys(photo.categories.dreamstime).length === 3
        && photo.headline.split(' ').length > 5
        && photo.caption.split(' ').length > 10
        && photo.keywords.length >= 7
        && photo.keywords.length <= 80
    } catch (err) {
      return false
    }
  }
}
