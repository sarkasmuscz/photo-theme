import actions from './actions'
import getters from './getters'
import mutations from './mutations'

export default {
  namespaced: true,

  state: {
    options: {
      /**
       * List of selected stocks in format {"<nameOfStock>": true/false}
       * */
      selectedStocks: {}
    }
  },

  actions: actions,
  getters: getters,
  mutations: mutations
}
