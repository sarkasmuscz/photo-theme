// @flow

import type { SubmitOptionsType } from '../../../types/SubmitOptionsType.flow'

export default {
  /**
   * Updates selected stocks in store.
   * */
  UPDATE_OPTIONS (state: any, data: SubmitOptionsType): void {
    state.options.selectedStocks = JSON.parse(JSON.stringify(data.selectedStocks))
  }
}
