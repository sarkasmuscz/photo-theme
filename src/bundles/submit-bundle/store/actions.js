// @flow

import axios from 'axios'
import swal from 'sweetalert'
import ShutterstockValidator from '../../../bundles/submit-bundle/validators/shutterstock.validator'
import DreamstimeValidator from '../../../bundles/submit-bundle/validators/dreamstime.validator'
// Import types
import type { SubmitOptionsType } from '../../../types/SubmitOptionsType.flow'

export default {
  /**
   * Updates selected stocks in store.
   * */
  updateOptions ({commit}: any, data: SubmitOptionsType): void {
    commit('UPDATE_OPTIONS', data)
  },

  /**
   * Submits selected photos to selected stocks.
   * */
  async submitPhotos ({rootState, dispatch, state}: Object) {
    let validators = {
      shutterstock: new ShutterstockValidator(),
      dreamstime: new DreamstimeValidator()
    }

    let selectedStocks = JSON.parse(JSON.stringify(state.options.selectedStocks))

    let submitMap = {}
    let photosMap = {}
    let submitOk = true

    Object.keys(selectedStocks).map((stock) => {
      if (selectedStocks[stock]) {
        submitMap[stock] = rootState.photos.filter((photo) => {
          if (!validators[stock].validate(photo)
            && rootState.selectedPhotos.map(id => id - 1).indexOf(photo.id) !== -1) {
            submitOk = false
          }

          return validators[stock].validate(photo)
            && rootState.selectedPhotos.map(id => id - 1).indexOf(photo.id) !== -1
        }).map((photo) => {
          photosMap[photo.filenameId] = photo

          return photo.filenameId
        })
      }

      return null
    })

    if (!submitOk) {
      await swal({
        title: 'Issues with submitting',
        text: 'Some images could not be sent to few stocks because there miss or have bad metadata.',
        buttons: {
          cancel: 'Cancel whole submission',
          submitGood: 'Submit photos which are ok'
        }
      }).then(function (option) {
        submitOk = option === 'cancel' ? false : option === 'submitGood'
      })
    }

    if (submitOk) {
      try {
        await axios.post('http://localhost:2000/submit', {
          map: submitMap
        })

        Object.keys(submitMap).map((stock) => {
          submitMap[stock].map((filename) => {
            chrome.runtime.sendMessage(
              'abmgjhflkmhpoiaibbjlhhdckeonliee',
              {
                action: 'submit', stock: stock, filename: filename, data: {
                caption: photosMap[filename].caption,
                keywords: photosMap[filename].keywords,
                license: photosMap[filename].license,
                explicit: photosMap[filename].explicit,
                categories: photosMap[filename].categories,
                imageType: photosMap[filename].imageType
              }
              },
              async () => {
                if (photosMap[filename].phase === 'submit') {
                  await dispatch('updatePhotoMetadata', {
                    index: photosMap[filename].id,
                    change: 'phase',
                    value: 'gallery'
                  }, {root: true})
                }

                let stocks = photosMap[filename].stocks
                stocks.push({
                  code: stock,
                  name: stock,
                  status: 'pending'
                })
                await dispatch('updatePhotoMetadata', {
                  index: photosMap[filename].id,
                  change: 'stocks',
                  value: stocks
                }, {root: true})

                dispatch('updatePhotoMetadata', {
                  index: photosMap[filename].id,
                  change: 'lastVersion',
                  value: JSON.parse(JSON.stringify(photosMap[filename]))
                }, {root: true})
              }
            )

            return null
          })

          return null
        })
      } catch (error) {
        throw error
      }
    }
  }
}
