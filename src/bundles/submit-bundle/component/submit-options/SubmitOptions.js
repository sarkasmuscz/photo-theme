// @flow

/**
 * Component shown in right panel for selecting stocks to submit photos.
 *
 * @tutorial submitBundle
 *
 * @module Bundles/SubmitBundles/Component/SubmitOptions
 * */
export default {
  name: 'SubmitOptions',
  props: {
    stocks: {
      type: Object,
      required: true
    }
  },
  /**
   * Data.
   * */
  data () {
    return {
      selectedStocks: {},
      options: {
        selectedStocks: {}
      }
    }
  },
  methods: {
    /**
     * Synchronize selected stocks with store.
     * */
    changeOptions (): void {
      this.$store.dispatch('submitModule/updateOptions', this.options)
    },

    /**
     * Submits photos to selected stocks.
     * */
    submit (): void {
      this.$store.dispatch('submitPhotos')
    }
  },
  watch: {
    selectedStocks: {
      /**
       * When user selects/deselects stocks, calls changeOptions method.
       * */
      handler (newValue: { string: boolean }): void {
        this.options.selectedStocks = newValue
        this.changeOptions()
      },
      deep: true
    }
  }
}
