// @flow

import Upload from '../../../upload-bundle/view/upload/Upload.vue'

/**
 * The main menu of the application. Main navigation part of layout.
 * It uses router-links to navigate through app. On the right side it has dropdown to
 * out-app links like subscription, docs and also logout link and mainly upload window.
 *
 * @tutorial navigation
 *
 * @module Bundles/AppBundle/Component/CrxMenu
 * */
export default {
  name: 'CrxMenu',
  components: {
    Upload
  },
  /** Data */
  data (): {} {
    return {
      showModal: false
    }
  }
}
