import PhotoMetadataEditForm from '../../../gallery-bundle/component/photo-metadata-edit-form/PhotoMetadataEditForm.vue'
import SubmitOptions from '../../../submit-bundle/component/submit-options/SubmitOptions.vue'
import Reason from '../../../review-bundle/component/reason/Reason.vue'
import EditOptions from '../../../edit-bundle/component/edit-options/EditOptions.vue'
import Histogram from '../../../edit-bundle/component/histogram/Histogram.vue'

/**
 * The right panel of the application. Part of the app layout.
 * It renders panels according to the view.
 *
 * @module Bundles/AppBundle/Component/CrxRightWrapper
 * */
export default {
  name: 'CrxRightWrapper',
  components: {
    PhotoMetadataEditForm,
    SubmitOptions,
    Reason,
    EditOptions,
    Histogram
  }
}
