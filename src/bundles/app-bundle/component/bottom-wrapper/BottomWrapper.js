// @flow
import VersionPanel from '../../../gallery-bundle/component/version-panel/VersionPanel.vue'
// Import types.
import type { PhotoType } from '../../../../types/PhotoType.flow'

/**
 * This module currently serves only as a wrapper for VersionPanel. It is located on the bottom of the
 * application in gallery view.
 *
 * @tutorial wrappers
 *
 * @module Bundles/AppBundle/Component/BottomWrapper
 */
export default {
  name: 'BottomWrapper',
  components: {
    VersionPanel
  },
  computed: {
    /**
     * List of selected photos (IDs).
     * */
    selectedPhotos (): Array<PhotoType> {
      return this.$store.getters.selectedPhotos
    },

    /**
     * If only one photo is selected.
     * */
    single (): boolean {
      return this.selectedPhotos.length === 1
    }
  }
}
