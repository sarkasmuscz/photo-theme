/**
 * Wrapper of the main view (it renders router view).
 *
 * @module Bundles/AppBundle/Component/CrxContent
 * */
export default {
  name: 'CrxContent'
}
