// @flow

/**
 * This component works like a checkbox but it (will) have design which is not dependent on the checkbox element. This
 * component serves as a selecting box with label of option and allows switching between two states: on and off
 *
 * @tutorial clickableSelect
 *
 * @module Bundles/AppBundle/Component/ClickableSelect
 * */
export default {
  name: 'ClickableSelect',
  props: {
    /**
     * Name of the select - displays in label.
     * */
    name: {
      type: String,
      required: true
    },

    /**
     * Is the select checked?
     * */
    check: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  computed: {
    /**
     * Code of the select - used as identifier. It is a name in the kebab-case.
     * */
    code (): string {
      let name: string = this.name
      name = name.toLowerCase().replace(/\s+/, '-')

      return name
    },

    /**
     * State of the select, either on or off.
     * */
    checked: {
      /**
       * Getter.
       * */
      get (): boolean {
        return this.check
      },

      /**
       * Setter.
       * */
      set (value: boolean): void {
        this.$emit('update:chosen', this.code, value)
      }
    }
  }
}
