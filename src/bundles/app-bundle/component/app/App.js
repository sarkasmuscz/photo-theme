import CrxMenu from '../crx-menu/CrxMenu.vue'
import CrxContent from '../crx-content/CrxContent.vue'

/**
 * A main application wrapper.
 * @module Bundles/AppBundle/Component/App
 * */
export default {
  name: 'App',
  components: {
    CrxMenu,
    CrxContent
  }
}
