// @flow

import type { PhotoType } from '../../../types/PhotoType.flow'

/**
 * This mixin serves as a component for rendering photo compare.
 * @module CompareMixin
 * */
export default {
  name: 'CompareMixin',
  computed: {
    /**
     * Returns either compare is active or not. ✅❌❓
     * @return {Boolean}
     * */
    compare (): boolean {
      return this.$store.getters.compare
    },

    /**
     * Get versions which should be compared. 🍎🍏
     * @return {Array<Object>} versions.
     * */
    versionsToCompare (): Array<PhotoType> {
      return this.$store.getters.versionsToCompare
    }
  },
  methods: {
    /**
     * Cancels compare.
     */
    cancelCompare (): void {
      this.$store.dispatch('cancelCompareOfPhotoVersions')
    }
  }
}
