// @flow

import NumbersBetweenMixin from '../../../mixins/helpers/numbersBetweenMixin'

/**
 * This mixin provides for gallery components functionality of selecting one or many photos.
 * @module GalleryPhotoMixin
 * */
export default {
  name: 'GalleryPhotoMixin',
  mixins: [NumbersBetweenMixin],
  props: {
    /**
     * Array of selected photos (IDs)
     * @property {Array} selectedPhotos
     * */
    selectedPhotos: {
      type: Array,
      required: true
    }
  },
  computed: {
    /**
     * Is this photo currently selected?
     * */
    isSelected (): boolean {
      return this.selectedPhotos.indexOf(this.id) !== -1
    }
  },
  methods: {
    /**
     * Select JUST this photo or in other cases call relevant method.
     * It is activated by single click.
     * */
    select (event: {}): void {
      // Ctrl + Click - select this and keep selected photos.
      if (event.ctrlKey) {
        this.selectOther()

        // Shift + Click - select a row.
      } else if (event.shiftKey) {
        this.selectRow()

        // Single click - select JUST this photo.
      } else {
        this.$emit('select-photo', [this.id])
      }
    },

    /**
     * Select this photo and keep before selected photos selected.
     * It is activated by Ctrl + Click.
     * */
    selectOther (): void {
      let selectedPhotos = this.$store.getters.selectedPhotos.slice(0)
      let delPos = selectedPhotos.indexOf(this.id)

      // If this photo is already selected this cancels selection on it.
      if (delPos !== -1) {
        selectedPhotos.splice(delPos, 1)

        this.$emit('select-photo', selectedPhotos)
      } else {
        selectedPhotos.push(this.id)

        this.$emit('select-photo', selectedPhotos)
      }
    },

    /**
     * Select all images between selected image with minimum id and this photo.
     * It is activated by Shift + Click.
     * */
    selectRow (): void {
      let min: number = 100000000
      let max: number = 0
      let selectedPhotos: Array<number> = this.$store.getters.selectedPhotos

      // Find minimum and maximum.
      for (let selectedPhoto in selectedPhotos) {
        if (Object.prototype.hasOwnProperty.call(selectedPhotos, selectedPhoto)) {
          selectedPhoto = selectedPhotos[selectedPhoto]

          // This expression means: when iterated photo's id is lower than minimum.
          if (selectedPhoto < min) {
            // Minimum is a id.
            min = selectedPhoto

          }

          // Same here.
          if (selectedPhoto > max) {
            max = selectedPhoto

          }
        }
      }

      // If this photo is between minimum and maximum.
      if (this.id >= min && this.id <= max) {
        // Todo: take into account a difference between min/max and id.
        this.$emit('select-photo', this.getAllNumbersBetween(min, this.id))

        // If this photo is below the minimum.
      } else if (this.id < min) {
        this.$emit('select-photo', this.getAllNumbersBetween(this.id, max))

        // If this photo is above the maximum.
      } else if (this.id > max) {
        this.$emit('select-photo', this.getAllNumbersBetween(min, this.id))

      }
    }
  }
}
