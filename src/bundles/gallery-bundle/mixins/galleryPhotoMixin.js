// @flow

import SelectingPhotoMixin from './selectingPhotoMixin'

export default {
  name: 'GalleryPhotoMixin',
  mixins: [SelectingPhotoMixin],
  props: {
    /**
     * Url link to the image source.
     * @property {String} imageLink
     * */
    imageLink: {
      type: String,
      required: true
    },

    /**
     * Sum of all earnings from this photo.
     * @property {String} totalEarnings
     * */
    totalEarnings: {
      type: String,
      required: true
    },

    /**
     * List of stocks (as objects {status: 'activestate.photos[id]', name: 'Shutterstock'})
     * which has status with this photo.
     * @property {Array} stocks
     * */
    stocks: {
      type: Array,
      required: false
    },

    /**
     * ID of the photo.
     * @property {Number} id
     * */
    id: {
      type: Number,
      required: true
    },

    /**
     * Injected rating of the photo.
     * @property {Number} score
     * */
    score: {
      type: Number,
      required: false,
      default: 0,
      validator: (value: 0 | 1 | 2 | 3 | 4 | 5) => value >= 0 && value <= 5
    },

    ratingReadonly: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  computed: {
    /**
     * If photo has been modified (changed metadata) and haven't been submitted.
     * @return {Boolean}
     * */
    isModified (): boolean {
      if (this.$route.name === 'gallery') {
        return this.$store.getters.isModified(this.id)

      }

      return false
    }
  }
}
