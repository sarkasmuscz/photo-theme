// @flow

import type { FiltersType } from '../../../types/FiltersType.flow'
import type { PhotoType } from '../../../types/PhotoType.flow'

/**
 * This mixin provides functionality for filtering photos. It is used by [Gallery](Gallery) view.
 * @module FilterPhotoMixin
 * */
export default {
  name: 'FilterPhotoMixin',
  /**
   * Data of this mixin. FilterPhotos property is temporary variable to save filtered
   * photos for the next filter.
   * @return {Object} Properties.
   * */
  data () {
    return {
      // At the beginning inject all photos from gallery here.
      filterPhotos: this.photos
    }
  },
  methods: {
    /**
     * Push photos through all filters and return filtered photos.
     * @namespace
     * @param {Object} data - Filter options.
     * @param {String} data.rating.symbol - Symbol for comparing photo's rating (greater, lower and equal).
     * @param {Integer} data.rating.value - Value to compare with photo's rating.
     * @param {Array} data.stocks - All stock codes selected.
     * @param {Array} data.statuses - All statuses codes selected.
     * @return {Array} Filtered photos.
     * */
    filter (data: FiltersType): Array<PhotoType> {
      this.filterPhotos = this.photos

      // The filter pipeline.
      this.filterByRating(data.rating)
        .filterByStocks(data.stocks)
        .filterByStatuses(data.statuses)
        .filterByCollection(data.collection)

      return this.filterPhotos
    },

    // Fixme: Why in type it is rating and not value
    /**
     * Filters photos by rating.
     *
     * @param {Object} ratingData - Contains symbol and value for rating.
     * @return {Object} This object.
     * */
    filterByRating (ratingData: { symbol?: 'greaterThan' | 'lowerThan' | 'equal', value: 0 | 1 | 2 | 3 | 4 | 5 }): {} {
      // Check if the rating data is defined and it is not empty object.
      if (ratingData === undefined || ratingData.symbol === undefined) {
        return this
      }

      let returnPhotos: Array<PhotoType> = []
      let photos: Array<PhotoType> = this.filterPhotos

      // Go through photos.
      for (let photo in photos) {
        if (Object.prototype.hasOwnProperty.call(photos, photo)) {
          photo = photos[photo] // This gets object of the photo by key.

          // Go through cases of rate symbol.
          switch (ratingData.symbol) {
            case 'greaterThan':
              if (photo.rating >= ratingData.value) {
                returnPhotos.push(photo)

              }
              break
            case 'lowerThan':
              if (photo.rating <= ratingData.value) {
                returnPhotos.push(photo)

              }
              break
            case 'equal':
              if (photo.rating === ratingData.value) {
                returnPhotos.push(photo)

              }
              break
            default:

          }
        }
      }

      // Save filter results.
      this.filterPhotos = returnPhotos

      return this
    },

    /**
     * Filters photos by stocks.
     *
     * @param {Array} stocksData - Contains stock codes.
     * @return {Object} This object.
     * */
    filterByStocks (stocksData: Array<string>): {} {
      // Checks if stockData is not empty.
      if (!(stocksData && stocksData.length)) {
        return this
      }

      let returnPhotos: Array<PhotoType> = []
      let photos: Array<PhotoType> = this.filterPhotos

      // Go through photos.
      for (let photo in photos) {
        if (Object.prototype.hasOwnProperty.call(photos, photo)) {
          photo = photos[photo] // Get photo object.

          // Go through stock entries of the current photo.
          for (let stock in photo.stocks) {
            if (Object.prototype.hasOwnProperty.call(photo.stocks, stock)) {
              stock = photo.stocks[stock] // Get stock object.

              // Check if stocksData contains stock code of the photo - if the photo has entry about this stock.
              if (stocksData.indexOf(stock.code) !== -1) {
                returnPhotos.push(photo)

                break

              }
            }
          }
        }
      }

      // Save filter results.
      this.filterPhotos = returnPhotos

      return this
    },

    /**
     * Filter photos by statuses.
     *
     * @param {Array} statusesData - Contains statuses codes.
     * @return {Object} This object.
     * */
    filterByStatuses (statusesData: Array<string>): {} {
      // Checks if statusesData is not empty.
      if (!(statusesData && statusesData.length)) {
        return this
      }

      let returnPhotos: Array<PhotoType> = []
      let photos: Array<PhotoType> = this.filterPhotos

      // Go through photos.
      for (let photo in photos) {
        if (Object.prototype.hasOwnProperty.call(photos, photo)) {
          photo = photos[photo] // Get the photo object from key.

          // Go through stocks
          for (let stock in photo.stocks) {
            if (Object.prototype.hasOwnProperty.call(photo.stocks, stock)) {
              stock = photo.stocks[stock] // Get the stock object from key

              // Go through statuses from statusesData
              for (let status in statusesData) {
                if (Object.prototype.hasOwnProperty.call(statusesData, status)) {
                  status = statusesData[status] // Get the status object from key

                  // Compare if one of the stock has this status on this photo.
                  if (stock.status === status) {
                    returnPhotos.push(photo)

                    // Break cycle because it is not needed to check other
                    // Statuses for single stock (one stock - one status).
                    break
                  }
                }
              }
            }
          }
        }
      }

      // Save filter result.
      this.filterPhotos = returnPhotos

      return this
    },

    /**
     * Filters photos by collections which they belong to.
     * @param {Integer} collection - Index of the collection in the store state.
     * @return {Object} This object.
     * */
    filterByCollection (collection: number): {} {
      // Checks if the collection is undefined or null - if the filter should run.
      if (collection === null || typeof collection === 'undefined') {
        return this
      }

      let returnPhotos: Array<PhotoType> = []
      let photos: Array<PhotoType> = this.filterPhotos

      // Go through photos.
      for (let photo in photos) {
        if (Object.prototype.hasOwnProperty.call(photos, photo)) {
          photo = photos[photo] // Get photo object.

          // Go through collection's photo.
          for (let colPhoto in this.$store.state.collections[collection].photos) {
            if (Object.prototype.hasOwnProperty.call(this.$store.state.collections[collection].photos, colPhoto)) {
              // Get index of the photo - this is index of the photo in the state.photos.
              colPhoto = this.$store.state.collections[collection].photos[colPhoto]

              // Is photo in the collection also filtered photo - it wasn't filtered by another filter before?
              if (this.photos[colPhoto] === photo) {
                returnPhotos.push(photo)

              }
            }
          }
        }
      }

      this.filterPhotos = returnPhotos

      return this
    }
  }
}
