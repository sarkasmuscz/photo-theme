// @flow

import SortLogicMixin from './sortLogicMixin'

/**
 * This mixin provides functionality for sorting photos in gallery.
 *
 * @module Bundles/GalleryBundle/Mixins/SortPhotosMixin
 * */
export default {
  name: 'SortPhotosMixin',
  mixins: [
    SortLogicMixin
  ],
  /**
   * Data of this mixin. FilterPhotos property is temporary variable to save filtered
   * photos for the next filter.
   * */
  data (): { sort: 'none' | 'by-date' | 'by-rating' } {
    return {
      /**
       * Sort algorithm selected. Either by-date, by-rating or none.
       * */
      sort: 'none'
    }
  },
  methods: {
    /**
     * Updates sort data. This is handler of event from FilterAssignPanel.
     * */
    updateSort (sort: string): void {
      this.sort = sort
    }
  }
}
