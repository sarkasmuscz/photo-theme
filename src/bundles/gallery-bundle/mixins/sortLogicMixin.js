// @flow

import type { PhotoType } from '../../../types/PhotoType.flow'

export default {
  name: 'SortLogicMixin',
  methods: {
    /**
     * Sorts all photos objects injected in param.
     * @param {Array} photos
     * @return {Array} sorted photos.
     * */
    sortPhotos (photos: Array<PhotoType>): Array<PhotoType> {
      if (this.sort !== 'none') {
        let sort = 'sort-' + this.sort
        sort = sort.replace(/-([a-z])/g, function (g) {
          return g[1].toUpperCase()
        })

        return this[sort](photos)

      }

      return photos
    },

    /**
     * Sorts photos objects by date.
     * @param {Array} photos
     * @return {Array} sorted photos.
     * */
    sortByDate (photos: Array<PhotoType>): Array<PhotoType> {
      let returnPhotos = []
      let lowestDate = new Date()
      let lowestDatePhoto

      for (let i = 0; i < photos.length; i++) {
        let slicedPhotos = photos.filter(
          function (item) {
            return returnPhotos.indexOf(item) === -1
          })

        for (let photo in slicedPhotos) {
          if (Object.prototype.hasOwnProperty.call(slicedPhotos, photo)) {
            photo = slicedPhotos[photo]
            let date = new Date(photo.date)

            if (date < lowestDate) {
              lowestDate = date
              lowestDatePhoto = photo
            }
          }
        }

        returnPhotos.push(lowestDatePhoto)
        lowestDatePhoto = null
        lowestDate = new Date()
      }

      return returnPhotos
    },

    /**
     * Sorts photos objects by rating.
     * @param {Array} photos
     * @return {Array} sorted photos.
     * */
    sortByRatings (photos: Array<PhotoType>): Array<PhotoType> {
      let photosByRating = [[], [], [], [], [], []]

      for (let photo in photos) {
        if (Object.prototype.hasOwnProperty.call(photos, photo)) {
          photo = photos[photo]
          photosByRating[photo.rating].push(photo)

        }
      }

      let returnPhotos = []

      // Sorting from best to worst
      for (let rating = 5; rating >= 0; rating--) {
        returnPhotos = photosByRating[rating].concat(returnPhotos)

      }
      returnPhotos.reverse()

      return returnPhotos
    }
  }
}
