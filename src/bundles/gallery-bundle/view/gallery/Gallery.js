// @flow

import GalleryPhoto from '../../component/gallery-photo/GalleryPhoto.vue'
import SortPanel from '../../component/sort-panel/SortPanel.vue'
import FilterAssignPanel from '../../component/filter-assign-panel/FilterAssignPanel.vue'
import FilterPhotosMixin from '../../mixins/filterPhotosMixin'
import SortPhotosMixin from '../../mixins/sortPhotosMixin'
import ContextMenu from 'vue-context-menu'
import BottomWrapper from '../../../app-bundle/component/bottom-wrapper/BottomWrapper.vue'
import ComparePanel from '../../component/compare-panel/ComparePanel.vue'
import CompareMixin from '../../mixins/compareMixin'
import CrxRightWrapper from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'
import SubmitOptions from '../../../submit-bundle/component/submit-options/SubmitOptions.vue'
import swal from 'sweetalert'

/**
 * At this moment the main view of the application. It shows all the images with it's data and provides functionalities
 * to edit these data. 🖼🖼📷
 * @module Gallery
 * */
export default {
  name: 'Gallery',
  mixins: [
    // This mixin is used for filtering the photos.
    FilterPhotosMixin,

    SortPhotosMixin,

    CompareMixin
  ],
  components: {
    CrxRightWrapper,
    GalleryPhoto,
    ContextMenu,
    SortPanel,
    FilterAssignPanel,
    BottomWrapper,
    ComparePanel,
    SubmitOptions
  },
  /**
   * Data.
   */
  data () : Object {
    return {
      showModal: false
    }
  },
  /**
   * Created hook. It sets up defaults filters every time gallery is rerendered.
   * */
  created () : void {
    this.$store.dispatch('updateFilters', {
      collection: null,
      rating: {},
      stocks: [],
      statuses: []
    })

    this.$store.dispatch('deselectAllPhotos')
  },
  computed: {
    /**
     * All photos in the store.
     * @property {Array} photos
     * */
    photos () : Array<Object> {
      return this.$store.getters.photos('gallery').filter((photo) => {
        return photo !== undefined && photo.deleted !== undefined && !photo.deleted
      })
    },

    /**
     * Filtered photos by filter panel options.
     * @property {Array} filteredPhotos
     * */
    filteredPhotos () : Array<Object> {
      return this.filter(this.$store.getters.getFilters)
    },

    /**
     * Sorted and filtered photos which are ready to render in view.
     * @property {Array} galleryPhotos
     * */
    galleryPhotos () : Array<Object> {
      return this.sortPhotos(this.filteredPhotos)
    }
  },
  methods: {
    /**
     * Rates selected photos. Event handler of FilterAssignPanel panels.
     * @param {number} value - Value of the rating.
     * */
    filterAssignPanelRate (value: number): void {
      this.$store.dispatch('updateSelectedPhotoMetadata', {
        'change': 'rating',
        'value': value
      })
    },

    /**
     * Rates photo. Event handler of GalleryPhoto panels.
     * @param {number} value - Value of the rating.
     * @param {number} id - Id of the photo.
     * */
    galleryPhotoRate (value: number, id: number): void {
      this.$store.dispatch('ratePhoto', {
        'id': id - 1,
        'value': value
      })
    },

    // Fixme: Dont use galleryName.
    /**
     * Selects photo/photos. Event handler for GalleryPhoto panels.
     * @param {Array<number>} selected - List of photos to select.
     * */
    galleryPhotoSelect (selected: Array<number>): void {
      this.$store.dispatch('selectPhotos', {
        galleryName: 'gallery',
        photos: selected
      })
    },

    /**
     * Removes selected photos from evidence. ♻
     * */
    removePhoto (): void {
      swal({
        title: 'Remove selected photos?',
        text: 'Do you really wanna remove all currently selected photos?',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('removeSelectedPhotos')

        }
      })
    },

    /**
     * Toggles modal panel/window with submitOptions panel to submit photos to new stocks.
     * */
    toggleModal (): void {
      this.showModal = !this.showModal
    },

    /**
     * Updates submit options. Fixme: ❗❗THIS IS DUPLICATE❗❗
     * */
    updateSubmitOptionsInStore (options: Object): void {
      this.$store.dispatch('submitModule/updateOptions', options)
    },

    /**
     * Saves preset. Fixme: ❗❗THIS IS DUPLICATE❗❗
     * */
    saveSubmitOptionsPresetInTheStore (presetName: string): void {
      this.$store.dispatch('submitModule/savePreset', {presetName: presetName})
    },

    /**
     * Commits selected modified photos.
     */
    commitChanges (): void {
      swal({
        title: 'Commit changes?',
        text: 'Do you really wanna commit all changes on this photo? This will apply changes online on stocks.',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('commitChanges')
        }
      })
    },

    // Todo: this is probably duplicate.
    /**
     * Reverts all selected photos.
     * */
    revertPhotos (): void {
      swal({
        title: 'Revert photo?',
        text: 'Do you really wanna revert all changes on this photo?',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('revert')

        }
      })
    },

    /**
     * Creates a new version of base photo.
     * */
    createNewVersion (): void {
      this.$store.dispatch('createNewVersion')
    },

    downloadLatest (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}`
    },

    downloadRaw (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}/raw`
    }
  }
}
