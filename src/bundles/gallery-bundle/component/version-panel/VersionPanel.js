// @flow

import SelectedVersionsError from './errors/SelectedVersionsError'
import ContextMenu from 'vue-context-menu'
import Version from '../version/Version.vue'

/**
 * @module Bundles/GalleryBundle/Component/VersionPanel
 * */
export default {
  name: 'VersionPanel',
  components: {
    ContextMenu,
    Version
  },
  props: {
    /**
     * Currently selected photo.
     * */
    photo: {
      type: Object,
      required: false,
      default: {}
    },

    /**
     * List of all selected photos.
     * */
    selectedPhotos: {
      type: Array,
      required: true
    },

    /**
     * List of all selected versions (currently it is possible to select just 1 version).
     * */
    selectedVersions: {
      type: Array,
      required: true
    }
  },
  /**
   * Data🤷.
   */
  data (): {} {
    return {
      contextVersion: null
    }
  },
  methods: {
    /**
     * Emits select event when one of registered version has been selected.
     *
     * @throws TypeError - When param version is not either array or integer.
     * */
    select (versions: Array<number> | number): void {
      if (Array.isArray(versions) || versions === parseInt(versions)) {
        let returnVersions = typeof versions === 'number'
          ? [versions]
          : versions

        this.$store.dispatch('selectPhotoVersions', {
          photo: this.selectedPhotos[0] - 1,
          versions: returnVersions
        })

      } else {
        throw new TypeError('()')

      }
    },

    /**
     * Emits merge event when user selected merge option in context menu of one of registered versions.
     *
     * @throws TypeError - When version param is not integer.
     * @throws SelectedVersionsError - When multiple or none versions are selected.
     * */
    merge (version: number): void {
      if (version === parseInt(version)) {
        if (this.selectedVersions.length === 1 && this.selectedVersions.indexOf(version + 1) === -1) {
          this.$store.dispatch('mergePhotoVersions', {
            photo: this.selectedPhotos[0] - 1,
            primary: version + 1,
            secondary: this.selectedVersions[0]
          })

        } else {
          throw new SelectedVersionsError()

        }

      } else {
        throw new TypeError()

      }
    },

    /**
     * Emits compare event when user selected compare option in context menu of one of registered versions.
     * Compare makes new view in the gallery and compares two versions like images and compare their metadata.
     *
     * @throws TypeError - When version param is not integer.
     * @throws SelectedVersionsError - When multiple or none versions are selected.
     * */
    compare (version: number): void {
      if (version === parseInt(version)) {
        if (this.selectedVersions.length === 1 && this.selectedVersions.indexOf(version + 1) === -1) {
          this.$store.dispatch('comparePhotoVersions', {
            photo: this.selectedPhotos[0] - 1,
            primary: version + 1,
            secondary: this.selectedVersions[0]
          })

        } else {
          throw new SelectedVersionsError()

        }

      } else {
        throw new TypeError()

      }
    },

    /**
     * Commits version changes.
     * */
    commit (): void {
      this.$store.dispatch('commitVersionChanges')
    },

    /**
     * Submit versions.
     * */
    submit (): void {
      this.$parent.$parent.toggleModal()
    },

    /**
     * Remove version.
     * */
    remove (): void {
      this.$store.dispatch('removeVersion')
    },

    /**
     * Revert version.
     * */
    revert (): void {
      this.$store.dispatch('revertVersion')
    },

    /**
     * Emits rate event when user rates a version.
     * */
    rate (version: number, value: 0 | 1 | 2 | 3 | 4 | 5): void {
      this.$store.dispatch('ratePhotoVersion', {
        photo: this.photo.id,
        version: version,
        value: value
      })
    },

    /**
     * 👾👾 Assigns id to contextVersion - when working with context panel of versions, use this id to manipulate with it.
     * */
    setContextVersion (id: number): void {
      this.contextVersion = id
    }
  }
}
