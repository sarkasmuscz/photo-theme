export default class SelectedVersionsError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'SelectedVersionsError'
  }
}
