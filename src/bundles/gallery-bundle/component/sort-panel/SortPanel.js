// @flow

import RangeSlider from 'vue-range-slider'
import 'vue-range-slider/dist/vue-range-slider.css'
import EnumError from '../../../../core/errors/EnumError'

/**
 * This panel is placed at the top of the gallery. It shows fields to change view of the gallery. It has options to
 * set size of photos in gallery to change the view (boxed, tabled), and to sort photos.
 *
 * @module Bundles/GalleryBundle/Component/SortPanel
 * */
export default {
  name: 'SortPanel',
  components: {
    RangeSlider
  },
  /**
   * Non-reactive data for this component.
   * @return {Object} data
   * */
  data (): {} {
    return {
      /**
       * Current value of the sort select input. Either none, by-rating or by-date.
       * @property {String} sortData
       * */
      sortData: 'none'
    }
  },
  computed: {
    /**
     * Reactive sort prop of sort select input. It is used in v-model, because we have to avoid mutating props.
     * @property {String} sort
     * */
    sort: {
      /**
       * Getter.
       * */
      get (): string {
        return this.sortData
      },

      /**
       * Setter.
       * */
      set (value: string): void {
        if (value === 'none' || value === 'by-date' || value === 'by-ratings') {
          this.sortData = value
          this.$emit('sortChange', value)

        } else {
          throw new EnumError('(Component::SortPanel): Sort property is of type Enum. ' +
            'It excepts either none, by-date or by-ratings values.')

        }
      }
    }
  }
}
