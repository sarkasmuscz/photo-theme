// @flow

import StarRating from 'vue-star-rating'
import FilterPanel from '../filter-panel/FilterPanel.vue'
import CollectionsPanel from '../collections-panel/CollectionsPanel.vue'
import SortPanel from '../sort-panel/SortPanel.vue'
// Import types.
import type { FiltersType } from '../../../../types/FiltersType.flow'

/**
 * This panel is placed on the bottom of the gallery and has two main functionalities: it
 * shows data (raw size, resolution) about selected photo and assign/change filter values on
 * one or many selected photos.
 *
 * @module Bundles/GalleryBundle/Component/FilterAssignPanel
 * */
export default {
  name: 'FilterAssignPanel',
  components: {
    StarRating,
    FilterPanel,
    CollectionsPanel,
    SortPanel
  },
  props: {
    /**
     * List of selected photos (IDs).
     * @property {Array} selectedPhotos
     * */
    selectedPhotos: {
      type: Array,
      required: true
    },

    /**
     * List of all photos (Objects)
     * @property {Array} allPhotos
     * */
    allPhotos: {
      type: Array,
      required: true
    }
  },
  computed: {
    /**
     * Raw size of currently selected photo.
     * @return {String} rawSize
     * */
    rawSize (): string {
      if (this.selectedPhotos.length === 1) {
        return this.allPhotos[this.selectedPhotos[0] - 1].rawSize

      }

      // Return if many photos are selected.
      return '-'
    },

    /**
     * Resolution of currently selected photo.
     * @return {String} resolution
     * */
    resolution (): string {
      if (this.selectedPhotos.length === 1) {
        return this.allPhotos[this.selectedPhotos[0] - 1].resolution

      }

      // Return if many photos are selected.
      return '-'
    },

    /**
     * Score of currently selected photo/photos.
     * @property {Integer} score
     * */
    score: {
      /**
       * Getter.
       * */
      get (): number {
        if (this.selectedPhotos.length === 1) {
          return this.allPhotos[this.selectedPhotos[0] - 1].rating

        }

        // If many photos are selected, return 0 and allows to edit by setter.
        return 0
      },

      /**
       * Setter.
       * */
      set (value: number): void {
        this.$emit('rate', value)
      }
    }
  },
  methods: {
    /**
     * Todo: fix this come around.
     */
    updateSort (data: {}): void {
      this.$emit('updateSort', data)
    },

    /**
     * Updates store state for filters. This is event handler for collection-select event on collection panels.
     * @param {Number|null} collection - collection name or null if filtering is not active.
     * */
    collectionsFilter (collection: number): void {
      // Gets already existing filters object, because it mutate just a collection field.
      // This is needed because Vue has problems with observing adding keys on object.
      let filters = this.$store.getters.getFilters
      filters.collection = collection

      this.$store.dispatch('updateFilters', filters)
    },

    /**
     * Commits changes into the store.
     * @param {Object} data
     * */
    filterPanelFilter (data: FiltersType) {
      this.$store.dispatch('updateFilters', data)
    }
  }
}
