// @flow

import StarRating from 'vue-star-rating'
import GalleryPhotoMixin from '../../mixins/galleryPhotoMixin'

/**
 * Component which represents one image (last version of image in master branch)
 * and shows it's data. Also it provides selecting functionality through it's mixins.
 *
 * @module Bundles/GalleryBundle/Component/GalleryPhoto.
 * */
export default {
  name: 'GalleryPhoto',
  components: {
    StarRating
  },
  mixins: [GalleryPhotoMixin],
  computed: {
    /**
     * Reactive property used in <star-rating> v-model directive. It wraps a score prop.
     * @property {Integer} myScore
     * */
    myScore: {
      /**
       * Getter.
       * */
      get (): 0 | 1 | 2 | 3 | 4 | 5 {
        return this.score
      },

      /**
       * Setter.
       * */
      set (value: 0 | 1 | 2 | 3 | 4 | 5): void {
        this.$emit('rate-itself', value, this.id)
        this.$refs.rating.selectedRating = this.score
      }
    }
  },
  methods: {
    /**
     * Opens context menu for photo options.
     * */
    contextMenu () {
      // If multiple photos are not selected, then on right click select current photo.
      if (this.$store.getters.selectedPhotos.length < 2) {
        this.select({})

      }
      this.$parent.$refs.contextMenu.open()
    }
  }
}
