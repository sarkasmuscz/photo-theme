// @flow

import InputTag from 'vue-input-tag'
import swal from 'sweetalert'
// Import types.
import type { PhotoType } from '../../../../types/PhotoType.flow'
import type { CategoriesListType } from '../../../../types/CategoriesListType.flow'

/**
 * Component which represents [Gallery](Gallery) panel on the right side and serves as a manipulator of image's
 * metadata. It is a form not a panel (this panels is wrapped by CrxRightPanel). It can manipulate one photo or many
 * (with different form)
 *
 * @module Bundles/GalleryBundle/Component/PhotoMetadataEditForm
 * */
export default {
  name: 'PhotoMetadataEditForm',
  components: {
    InputTag
  },
  props: {
    /**
     * List of selected photos (IDs).
     * */
    selectedPhotos: {
      type: Array,
      required: true
    },

    /**
     * List of all photos.
     * @type {Array<PhotoType>}
     * */
    allPhotos: {
      type: Array,
      required: true
    },

    /**
     * All categories of all stocks available. This property serves as a list for
     * category fields panel.
     *
     * @type {CategoriesListType}
     * */
    categories: {
      type: Object,
      required: true
    },

    /**
     * List of stocks.
     * @type {Array<Stock>}
     * */
    stocks: {
      type: Object,
      required: true
    }
  },
  computed: {
    /**
     * Is selected just one photo or many?
     * */
    single () : boolean {
      return this.selectedPhotos.length === 1
    },

    /**
     * One selected photo.
     * */
    photo (): PhotoType {
      if (this.$store.getters.selectedVersions[this.selectedPhotos[0] - 1].length === 1) {
        return this.allPhotos[this.selectedPhotos[0] - 1].versions[
          this.$store.getters.selectedVersions[this.selectedPhotos[0] - 1][0] - 1
        ]

      }

      return this.allPhotos[this.selectedPhotos[0] - 1]
    },

    /**
     * Get categories of selected photo.
     * @property {Object} category
     * */
    category (): CategoriesListType {
      return this.photo.categories
    },

    license: {
      /**
       * Updates license of the selected photo.
       * @param {string} value select event.
       * */
      set (value: 'commercial' | 'editorial'): void {
        this.update('license', value)
      },

      /**
       * Returns license.
       * */
      get (): 'commercial' | 'editorial' {
        return this.photo.license
      }
    }
  },
  methods: {
    /**
     * Updates a headline of selected photo.
     * */
    updateHeadline (e: { target: { value: string } }): void {
      this.update('headline', e.target.value)
    },

    /**
     * Updates a caption of selected photo.
     * @param {Event} e Input event.
     * */
    updateCaption (e: { target: { value: string } }): void {
      this.update('caption', e.target.value)
    },

    /**
     * Updates keywords of selected photo. **Input tag input.**
     * */
    updateKeywords () : void {
      this.update('keywords', this.$refs.keywords.tags)
    },

    /**
     * Updates image type of selected photo.
     * @param {Event} e select event.
     * */
    updateImageType (e: { target: { value: 'photo' | 'illustration' } }): void {
      this.update('imageType', e.target.value)
    },

    /**
     * Updates location of the selected photo.
     * @param {Event} e input event.
     * */
    updateLocation (e: { target: { value: string } }): void {
      this.update('location', e.target.value)
    },

    /**
     * Updates explicit of the selected photo.
     * @param e check event.
     * */
    updateExplicit (e: { target: { value: 'nudity' | 'drugs', checked: boolean } }): void {
      // Clone the explicit object from the store and change checked value.
      let explicit = this.allPhotos[this.selectedPhotos[0] - 1].explicit
      explicit = Object.assign({}, explicit)
      explicit[e.target.value] = e.target.checked

      this.update('explicit', explicit)
    },

    /**
     * Updates categories of the selected photo.
     *
     * @param {string} stock to which stock category belongs.
     * @param {number} index which number the category is.
     * @param {event} e select event.
     * */
    updateCategories (stock: string, index: number, e: { target: { value: string } }): void {
      // Clone the categories object from the store and change selected value.
      let categories = JSON.parse(JSON.stringify(this.allPhotos[this.selectedPhotos[0] - 1].categories))
      let obj = {}
      obj[index - 1] = e.target.value
      categories[stock] = Object.assign(categories[stock] ? categories[stock] : {}, obj)

      this.update('categories', categories)
    },

    // Todo: Use Lodash here.
    /**
     * Dispatches update action for currently edited photo.
     * */
    update (change: string, value: any) {
      this.$store.dispatch('updateSelectedPhotoMetadata', {
        'change': change,
        'value': value
      })
    },

    /**
     * Sends photos to submit page.
     */
    sendToSubmit () : void {
      this.$store.dispatch('sendToSubmit')
    },

    /**
     * Reverts selected photo to original state (lastVersion object).
     */
    revert () : void {
      swal({
        title: 'Revert photo?',
        text: 'Do you really wanna revert all changes on this photo?',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('revert')

        }
      })
    }
  }
}
