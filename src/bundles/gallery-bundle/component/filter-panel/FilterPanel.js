// @flow

import StarRating from 'vue-star-rate'
import ClickableSelect from '../../../app-bundle/component/clickable-select/ClickableSelect.vue'

/**
 * Component which is rendered in [Gallery view](Gallery) and is used to filter photos shown in gallery
 * by filter options (ratings, stocks, statuses). It is rendered in [Left Panel](LeftPanel).
 * @module Bundles/GalleryBundle/Component/FilterPanel
 * */
export default {
  name: 'FilterPanel',
  components: {
    StarRating,
    ClickableSelect
  },
  props: {
    /**
     * All tracked stocks.
     * */
    stocks: {
      type: Object,
      required: true
    },

    /**
     * Active filters.
     * */
    filters: {
      type: Object,
      required: true
    }
  },
  computed: {
    /**
     * All tracked statuses
     * @return {Object} statuses
     * */
    statuses (): {} {
      return {active: 'Active', pending: 'Pending', rejected: 'Rejected'} // Todo: get this from Vuex store.
    }
  },
  /**
   * Non-reactive data for this component.
   * @return {Object} data
   * */
  data (): {} {
    return {
      /**
       * Selected stocks in format {shutterstock: true}
       * @property {Object} checked
       * */
      checked: {},

      /**
       * Selected statuses in format {rejected: true}
       * @property {Object} selectedStatuses
       * */
      selectedStatuses: {},

      /**
       * Selected value for filtering by rating.
       * @property {Integer} score
       * */
      score: 0,

      /**
       * Selected symbol for filtering by rating.
       * */
      rateSymbol: 'greaterThan'
    }
  },
  /**
   * Created hook. It resets filters in store and sets up properties.
   * */
  created (): void {
    for (let stock in this.stocks) {
      if (Object.prototype.hasOwnProperty.call(this.stocks, stock)) {
        this.checked[stock] = false

      }
    }

    for (let status in this.statuses) {
      if (Object.prototype.hasOwnProperty.call(this.statuses, status)) {
        this.selectedStatuses[status] = false

      }
    }
  },
  methods: {
    /**
     * Changes rate symbol by input from select element.
     * @param {Event} e - Select event.
     * */
    filterRateSymbol (e: { target: { value: string } }): void {
      this.rateSymbol = e.target.value
      this.updateFilters()
    },

    /**
     * Changes score by input from vue-star-rate component.
     * @param {Integer} value - Score value.
     * */
    filterRate (value: number): void {
      this.score = value
      this.updateFilters()
    },

    /**
     * Changes value of selection on one ClickableSelect - on stocks.
     *
     * @param {String} name - Stock name (code).
     * @param {Boolean} value - If it is checked or not.
     * */
    filterStocks (name: string, value: boolean): void {
      this.checked[name] = value
      this.updateFilters()
    },

    /**
     * Changes value of selection on one ClickableSelect - on statuses.
     *
     * @param {String} name - Status code.
     * @param {Boolean} value - If it is checked or not.
     * */
    filterStatus (name: string, value: boolean): void {
      this.selectedStatuses[name] = value
      this.updateFilters()
    },

    /**
     * Updates filters in the store.
     * */
    updateFilters (): void {
      let fStocks: Array<string> = []
      let fStatuses: Array<string> = []

      // Transfer selection objects into arrays ({shutterstock: true} -> ['shutterstock']).
      for (let stock: string in this.checked) {

        if (Object.prototype.hasOwnProperty.call(this.checked, stock)) {

          // Check if stock was checked in the panel.
          if (this.checked[stock] === true) {
            fStocks.push(stock)

          }
        }
      }

      // Transfer selection objects into arrays ({rejected: true} -> ['rejected']).
      for (let status: string in this.selectedStatuses) {

        if (Object.prototype.hasOwnProperty.call(this.selectedStatuses, status)) {

          // Check if status was checked in the panel.
          if (this.selectedStatuses[status] === true) {
            fStatuses.push(status)

          }
        }
      }

      let collection: number = this.filters.collection

      this.$emit('filter', {
        'rating': {
          'symbol': this.rateSymbol,
          'value': this.score
        },
        'stocks': fStocks,
        'statuses': fStatuses,
        'collection': collection
      })
    }
  }
}
