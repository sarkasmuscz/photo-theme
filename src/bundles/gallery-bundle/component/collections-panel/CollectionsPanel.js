// @flow

/**
 * The panel which shows all collections and allows to filter photos by that.
 * It is rendered in filter assign panel.
 *
 * @tutorial galleryBundle
 *
 * @module Bundles/GalleryBundle/Component/CollectionsPanel
 * */
export default {
  name: 'CollectionsPanel',
  props: {
    /**
     * All collections.
     * */
    collections: {
      type: Array,
      required: true
    },

    /**
     * Active filter options.
     * */
    filters: {
      type: Object,
      required: true
    }
  },
  methods: {
    /**
     * Updates store state for filters. This is triggered by selecting one of the
     * collection radio buttons.
     * */
    filterCollection (e: { target: { value: string } }): void {
      if (e.target.value !== 'all') {
        this.$emit('collection-select', e.target.value)

      } else {
        this.$emit('collection-select', null)

      }
    }
  }
}
