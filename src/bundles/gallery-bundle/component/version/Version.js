// @flow

import StarRating from 'vue-star-rating'

/**
 * This module represents one version. Its similar to GalleryPhoto.
 *
 * @module Bundles/GalleryBundle/Component/Version
 * */
export default {
  name: 'Version',
  components: {
    StarRating
  },
  props: {
    /**
     * Url link to the image source.
     * @property {String} imageLink
     * */
    imageLink: {
      type: String,
      required: true
    },

    /**
     * Sum of all earnings from this photo.
     * @property {String} totalEarnings
     * */
    totalEarnings: {
      type: String,
      required: true
    },

    /**
     * List of stocks (as objects {status: 'active', name: 'Shutterstock'})
     * which has status with this photo.
     * @property {Array} stocks
     * */
    stocks: {
      type: Array,
      required: false
    },

    /**
     * ID of the version.
     * @property {Number} id
     * */
    id: {
      type: Number,
      required: true
    },

    /**
     * Injected rating of the photo.
     * @property {Number} score
     * */
    score: {
      type: Number,
      required: false,
      default: 0,
      validator: (value: number) => value >= 0 && value <= 5
    },

    /**
     * Array of selected versions (IDs)
     * @property {Array} selectedPhotos
     * */
    selectedVersions: {
      type: Array,
      required: true
    },

    /**
     * List of selected photos (IDs).
     * */
    selectedPhotos: {
      type: Array,
      required: true
    }
  },
  computed: {
    /**
     * Reactive property used in <star-rating> v-model directive. It wraps a score prop.
     * @property {Integer} myScore
     * */
    myScore: {
      /**
       * Getter
       * */
      get (): 0 | 1 | 2 | 3 | 4 | 5 {
        return this.score
      },

      /**
       * Setter
       * */
      set (value: 0 | 1 | 2 | 3 | 4 | 5) {
        this.$emit('rate-itself', this.id, value)
        this.$refs.rating.selectedRating = this.score
      }
    },

    /**
     * Is this photo currently selected?
     * */
    isSelected (): boolean {
      return this.selectedVersions.indexOf(this.id) !== -1
    },

    /**
     * Checks if this version is modified.
     * */
    isModified (): boolean {
      return this.$store.getters.isModified(this.selectedPhotos[0], this.id)
    }
  },
  methods: {
    /**
     * Emits select-version event when user clicked on this component.
     * */
    select (): void {
      this.$emit('select-version', [this.id])
    },

    /**
     * Opens context menu.
     * */
    contextMenu (): void {
      if (this.selectedVersions.length === 0) {
        this.select()
      }

      this.$emit('context-menu-open', this.id - 1)
      this.$parent.$refs.contextMenuVersions.open()
    }
  }
}
