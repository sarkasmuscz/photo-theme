/**
 * This panel is rendered in GalleryBundle/View/GalleryView when triggered compare of two versions.
 *
 * @module Bundles/GalleryBundle/Component/ComparePanel
 * */
export default {
  name: 'ComparePanel',
  props: {
    /**
     * Versions to compare
     * */
    versions: {
      type: Array,
      required: true
    },

    /**
     * Photo which is base branch for these versions.
     * */
    photo: {
      type: Object,
      required: true
    }
  }
}
