// @flow

import CrxRightWrapper from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'
import GalleryPhoto from '../../../gallery-bundle/component/gallery-photo/GalleryPhoto.vue'
import ContextMenu from 'vue-context-menu'
import swal from 'sweetalert'
// Import types.
import type { PhotoType } from '../../../../types/PhotoType.flow'

export default {
  name: 'Metadata',
  components: {
    CrxRightWrapper,
    GalleryPhoto,
    ContextMenu
  },
  /**
   * Deselect all photos on initiation.
   * */
  created () {
    this.$store.dispatch('deselectAllPhotos')
  },
  computed: {
    /**
     * Returns list of filtered metadata photos.
     * */
    photos (): Array<PhotoType> {
      return this.$store.getters.photos('metadata').filter((photo) => {
        return photo !== undefined && photo.deleted !== undefined && !photo.deleted
      })
    }
  },
  methods: {
    /**
     * Selects photo/photos. Event handler for GalleryPhoto panels.
     * @param {Array} selected - List of photos to select.
     * */
    galleryPhotoSelect (selected: number): void {
      this.$store.dispatch('selectPhotos', {
        galleryName: 'gallery',
        photos: selected
      })
    },

    /**
     * Returns selected photos back to edit phase.
     * */
    sendToEdit (): void {
      this.$store.dispatch('updateSelectedPhotoMetadata', {
        change: 'phase',
        value: 'edit'
      })
    },

    /**
     * Removes selected photos from evidence. ♻
     * */
    removePhoto (): void {
      swal({
        title: 'Remove selected photos?',
        text: 'Do you really wanna remove all currently selected photos?',
        icon: 'warning',
        buttons: true
      }).then((ok) => {
        if (ok) {
          this.$store.dispatch('removeSelectedPhotos')

        }
      })
    },

    downloadLatest (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}`
    },

    downloadRaw (filename: string): void {
      window.location.href = `http://localhost:1025/download/${filename}/raw`
    }
  }
}
