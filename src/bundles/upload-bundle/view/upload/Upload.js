// @flow

import Vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.css'
import CrxRightPanel from '../../../app-bundle/component/crx-right-wrapper/CrxRightWrapper'

/**
 * Page for upload raw files.
 * @module Upload
 * */
export default {
  name: 'Upload',
  components: {
    vueDropzone: Vue2Dropzone,
    CrxRightPanel
  },
  /**
   * Returns config for dropzone.
   * */
  data (): {} {
    return {
      dropzoneOptions: {
        paramName: 'photo',
        url: 'http://localhost:3000/upload',
        maxFilesize: 50
      }
    }
  }
}
