// @flow

import type { SnapshotType } from '../../types/SnapshotType.flow.js'

/**
 * Little inner library to adjust photo lights, colors, sharpening and noise. This library is
 * used by EditBundle/Panels/CanvasPanel to render edited image by provided snapshot (edit options).
 * Editor gets imageData and returns edited imageData back.
 *
 * @tutorial snapshot
 * @tutorial editorBundle
 *
 * @module Bundles/EditorBundle/Editor
 * */
export default {
  /**
   * Core function. Gets injected image data and snapshot and drives editing process.
   * */
  edit (imgData: { data: Array<number> }, snapshot: SnapshotType) {
    // Iterate over pixels.
    for (let i = 0; i < imgData.data.length; i += 4) {

      // Create new pixel from red, green and blue values and apply adjustments on it.
      let pixelData = this.editPixel(imgData.data[i], imgData.data[i + 1], imgData.data[i + 2])
        .temperature(snapshot.temperature)
        .tint(snapshot.tint)
        .saturation(snapshot.saturation)
        .vibrance(snapshot.vibrance)
        .exposure(snapshot.exposure)
        .contrast(snapshot.contrast)

      // Set processed RGB values back.
      imgData.data[i] = pixelData.red
      imgData.data[i + 1] = pixelData.green
      imgData.data[i + 2] = pixelData.blue
    }

    return imgData
  },

  /**
   * Creates pixel module.
   * */
  editPixel (red: number, green: number, blue: number) {
    let rgbToHsl = this.RGBToHSL
    let hslToRgb = this.HSLToRGB

    // Pixel module.
    return {
      red: red,
      green: green,
      blue: blue,

      /**
       * Adjusts temperature.
       *
       * @see http://www.tannerhelland.com/5675/simple-algorithms-adjusting-image-temperature-tint/
       * */
      temperature (adjustmentValue: number) {
        this.red = Math.min(this.red + adjustmentValue, 255)
        this.blue = Math.max(this.blue - adjustmentValue, 0)

        return this
      },

      /**
       * Adjusts temperature.
       *
       * @see http://www.tannerhelland.com/5675/simple-algorithms-adjusting-image-temperature-tint/
       * */
      tint (adjustmentValue: number) {
        this.green = Math.min(this.green + adjustmentValue, 255)

        return this
      },

      /**
       * Adjusts saturation
       *
       * @see https://cs.wikipedia.org/wiki/HSV
       * */
      saturation (adjustmentValue: number) {
        let HSL = rgbToHsl(this.red, this.green, this.blue)

        HSL.saturation = HSL.saturation * ((adjustmentValue + 50) / 100) * 2

        let RGB = hslToRgb(HSL.hue, HSL.saturation, HSL.lightness)
        this.red = RGB.red
        this.green = RGB.green
        this.blue = RGB.blue

        return this
      },

      /**
       * Currently vibrance does the same as saturation.
       *
       * @todo Create vibrance algorithm as well.
       * */
      vibrance (adjustmentValue: number) {
        let HSL = rgbToHsl(this.red, this.green, this.blue)

        HSL.saturation = HSL.saturation * ((adjustmentValue + 50) / 100) * 2

        let RGB = hslToRgb(HSL.hue, HSL.saturation, HSL.lightness)
        this.red = RGB.red
        this.green = RGB.green
        this.blue = RGB.blue

        return this
      },

      /**
       * ACTUALLY BRIGHTNESS.
       * */
      exposure (adjustmentValue: number) {
        // I'm like WTF, this is working???
        this.red = this.red + adjustmentValue
        this.blue = this.blue + adjustmentValue
        this.green = this.green + adjustmentValue

        return this
      },

      /**
       * Adjusts contrast.
       *
       * @see https://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-5-contrast-adjustment/
       * */
      contrast (adjustmentValue: number) {
        let factor = 259 * (adjustmentValue + 255) / (255 * (259 - adjustmentValue))

        this.red = factor * (this.red - 128) + 128
        this.green = factor * (this.green - 128) + 128
        this.blue = factor * (this.blue - 128) + 128

        return this
      }
    }
  },

  /**
   * Converts RGB model to HSL
   *
   * @see https://cs.wikipedia.org/wiki/HSV
   * */
  RGBToHSL (red: number, green: number, blue: number) {
    let min = Math.min(red, green, blue)
    let max = Math.max(red, green, blue)
    let diff = max - min

    let hue = 0
    let saturation = 0
    let lightness = (min + max) / 2

    if (diff !== 0) {
      if (max === red && green >= blue) {hue = 60 * (green - blue) / (max - min)}
      if (max === red && green < blue) {hue = 60 * (green - blue) / (max - min) + 360}
      if (max === green) {hue = 60 * (blue - red) / (max - min) + 120}
      if (max === blue) {hue = 60 * (red - green) / (max - min) + 240}

      if (lightness <= 128) {saturation = (max - min) / (max + min)}
      if (lightness >= 128) {saturation = (max - min) / (512 - (max + min))}
      if (lightness === 0) {saturation = 0}
    }

    return {
      hue: hue,
      saturation: saturation * 100, // To percentage
      lightness: lightness / 2.56 // To percentage (lightness is in bits)
    }
  },

  /**
   * Converts HSL model to RGB
   *
   * @see https://cs.wikipedia.org/wiki/HSV
   * */
  HSLToRGB (hue: number, saturation: number, lightness: number) {
    if (saturation === 0) {
      return {
        red: lightness * 2.56,
        green: lightness * 2.56,
        blue: lightness * 2.56
      }
    }

    let q = 0
    let tempLightness = lightness / 100
    let tempSaturation = saturation / 100

    if (lightness >= 50) {q = tempLightness + tempSaturation - tempLightness * tempSaturation}
    if (lightness < 50) {q = tempLightness * (1 + tempSaturation)}

    let p = 2 * tempLightness - q

    let hueNormalized = hue / 360

    let temp = value => {
      if (value < 0) {
        return value + 1
      }
      if (value > 1) {
        return value - 1
      }

      return value
    }

    let tempR = temp(hueNormalized + 1 / 3)
    let tempG = temp(hueNormalized)
    let tempB = temp(hueNormalized - 1 / 3)

    let RGB = [tempR, tempG, tempB]

    for (let i = 0; i < 3; i++) {
      RGB[i] = RGB[i] < 1 / 6 ? p + (q - p) * 6 * RGB[i]
        : RGB[i] < 1 / 2 ? q
          : RGB[i] < 2 / 3 ? p + (q - p) * 6 * (2 / 3 - RGB[i])
            : p
    }

    return {
      red: RGB[0] * 256,
      green: RGB[1] * 256,
      blue: RGB[2] * 256
    }
  }
}
