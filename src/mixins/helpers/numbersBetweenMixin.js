/**
 * This mixin provides helper for getting numbers between two extreme numbers.
 * @module NumbersBetweenMixin
 * */
export default {
  name: 'NumbersBetweenMixin',
  methods: {
    // Todo: use declarative code
    /**
     * Finds all integers between x and y.
     * The x MUST be lower than y.
     * */
    getAllNumbersBetween (x, y) {
      let min = x
      let max = y
      let numbers = []

      // Swaps numbers if x is larger than y.
      if (x > y) {
        max = [min, min = max][0]

      }

      for (let i = min; i <= max; i++) {
        numbers.push(i)

      }

      return numbers
    }
  }
}
