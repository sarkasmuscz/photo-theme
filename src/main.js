// @flow

import Vue from 'vue'
import App from './bundles/app-bundle/component/app/App.vue'
import VuexStore from './main/vuex.store'
import VueRouter from './main/vue.router'
import VueModules from 'vue-modules-plugin'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

/*If (process.env.NODE_ENV !== 'development') {
  Raven
    .config('https://69dcece4b00a4ddc9d350854338afa55@sentry.io/274605')
    .addPlugin(RavenVue, Vue)
    .install()

  Raven.setTagsContext({
    environment: process.env.NODE_ENV
  })

  // Todo: add real context data.
  Raven.setUserContext({
    id: 1,
    username: 'michal.doubek',
    email: 'michal@doubkovi.cz'
  })
 }*/

Vue.use(VueModules)

let store = VuexStore.store
let router = VueRouter.router

/*SetInterval(() => {
 store.state.submitter.submitData()
 }, 5000)*/

Vue.filter('capitalize', function (value) {
  if (!value) {
    return ''
  }

  value = value.toString()

  return value.charAt(0).toUpperCase() + value.slice(1)
})

// Create and mount the root instance.
// Make sure to inject the router with the router option to make the whole app router-aware.
new Vue({
  router,
  store,
  components: {
    App
  }
}).$mount('#main')
