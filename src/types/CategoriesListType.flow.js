// @flow

import type { CategoryType } from './CategoryType.flow'

/**
 * @module Types.CategoriesListType
 */
export type CategoriesListType = {
  string?: {
    min?: number,
    max?: number,
    categories: Array<CategoryType>
  }
}
