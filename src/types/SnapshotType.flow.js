// @flow

/**
 * @module Types.SnapshotType
 */
export type SnapshotType = {
  temperature: number,
  tint: number,
  exposure: number,
  contrast: number,
  highlights: number,
  shadows: number,
  blacks: number,
  whites: number,
  clarity: number,
  vibrance: number,
  saturation: number,
  sharpening: {
    level: number,
    radius: number,
    detail: number
  },
  noiseReduction: {
    level: number,
    detail: number
  },
  lensCorrection: false
}

