// @flow

/**
 * @module Types.CategoryType
 */
export type CategoryType = {
  name: string,
  code: string
}
