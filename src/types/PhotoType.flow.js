// @flow

import type { StockType } from './StockType.flow'
import type { SnapshotType } from './SnapshotType.flow'

/**
 * @module Types.PhotoType
 */
export type PhotoType = {
  phase: string,
  id: number,
  imageLink: string,
  totalEarnings: string,
  imageType: 'illustration' | 'photo',
  categories: Array<StockType>,
  stocks: Array<StockType>,
  headline: string,
  caption: string,
  keywords: Array<string>,
  location: string,
  license: 'commercial' | 'editorial',
  explicit: { nudity: boolean, drugs: boolean },
  rating: 0 | 1 | 2 | 3 | 4 | 5,
  date: string,
  rawSize: string,
  resolution: string,
  deleted?: boolean,
  snapshot: SnapshotType,
  versions?: Array<PhotoType>,
  lastVersion?: PhotoType
}
