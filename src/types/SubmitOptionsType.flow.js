// @flow

export type SubmitOptionsType = {
  selectedStocks: {
    string?: boolean
  }
}
