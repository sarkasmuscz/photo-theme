// @flow

/**
 * @module Types.StockType
 */
export type StockType = {
  name: string,
  code: string,
  status?: 'active' | 'pending' | 'rejected'
}
