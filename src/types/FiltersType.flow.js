// @flow

/**
 * @module Types/FiltersType
 */
export type FiltersType = {
  collection: ?number,
  rating: {
    symbol?: 'greaterThan' | 'lowerThan' | 'equal',
    rating?: 0 | 1 | 2 | 3 | 4 | 5
  },
  stocks: Array<string>,
  statuses: Array<string>
}
