import Vue from 'vue'
import VueRouter from 'vue-router'
import Gallery from '../bundles/gallery-bundle/view/gallery/Gallery.vue'
import Review from '../bundles/review-bundle/view/review/Review.vue'
import Submit from '../bundles/submit-bundle/view/submit/Submit.vue'
import Metadata from '../bundles/metadata-bundle/view/metadata/Metadata.vue'
import Edit from '../bundles/edit-bundle/view/edit/Edit.vue'
// Fixme: bad name of view.
import InitView from '../bundles/init-bundle/view/init-view/InitView.vue'
import Preferences from '../bundles/preferences-bundle/view/preferences/Preferences.vue'

Vue.use(VueRouter)

/**
 * Routes
 * @property {Array.<Route>} routes
 * */
const routes = [
  {
    path: '/',
    name: 'gallery',
    component: Gallery
  },
  {
    path: '/gallery',
    name: 'gallery',
    component: Gallery
  },
  {
    path: '/edit',
    name: 'edit',
    component: Edit
  },
  {
    path: '/single-edit/:id',
    name: 'single-edit',
    component: Edit
  },
  {
    path: '/review-edit/:id',
    name: 'review-edit',
    component: Edit
  },
  {
    path: '/version-edit/:photoId/:versionId',
    name: 'version-edit',
    component: Edit
  },
  {
    path: '/review',
    name: 'review',
    component: Review
  },
  {
    path: '/submit',
    name: 'submit',
    component: Submit
  },
  {
    path: '/metadata',
    name: 'metadata',
    component: Metadata
  },
  {
    path: '/init',
    name: 'init',
    component: InitView
  },
  {
    path: '/preferences',
    name: 'preferences',
    component: Preferences
  }
]

// Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// Keep it simple for now.
const router = new VueRouter({
  routes // Short for `routes: routes`
})

export default {
  name: 'VueRouter',
  router: router
}
