import Vue from 'vue'
import Vuex from 'vuex'
import getters from './store/getters'
import mutations from './store/mutations'
import actions from './store/actions'
// Import modules
import upload from '../bundles/upload-bundle/store/upload-bundle'
import submit from '../bundles/submit-bundle/store/submit-bundle'
import review from '../bundles/review-bundle/store/review-bundle'
import edit from '../bundles/edit-bundle/store/edit-bundle'

Vue.use(Vuex)

// Defining a Vuex store with testing data.
const store = new Vuex.Store({
  strict: process.env.NODE_ENV === 'development',

  modules: {
    upload: upload,
    // Todo: divide all functions to modules - this is the consecvence.
    submitModule: submit,
    reviewModule: review,
    editModule: edit
  },

  state: window.VuexStore ? window.VuexStore : {
    /**
     * List of all photos which are tracked by this system.
     * @namespace
     * @property {Array} photos
     * @property {String} photos.imageLink - Source of the image
     * @property {String} photos.totalEarnings - Total earnings of the image from all stocks with currency symbol.
     * @property {String} photos.imageType - Either photo or illustration - type of image.
     * @property {Object} photos.categories - Selected categories.
     * @property {Array} photos.categories.<stock> - List of selected categories of single stock.
     * @property {Array} photos.stocks - List of stocks and statuses.
     * @property {String} photos.stocks[<index>].name - Name of the stock.
     * @property {String} photos.stocks[<index>].status - Status of the image on this stock.
     * @property {String} photos.headline - Headline of the image (IPTC metadata).
     * @property {String} photos.caption - Caption of the image (IPTC metadata).
     * @property {Array} photos.keywords - Keywords of the image (IPTC metadata).
     * @property {String} photos.location - Location where the image was taken (IPTC metadata).
     * @property {String} photos.license - Either commercial or editorial - license type.
     * @property {Object} photos.explicit - Explicit tags for the image with keys: nudity and drugs - these
     * tags are not needed to be all defined here - if they are not, they are implicitly `false`.
     * */
    photos: [
      {
        phase: 'gallery',
        id: 0,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$50',
        imageType: 'illustration',
        categories: {},
        stocks: [
          {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
        ],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: 'Home',
        license: 'commercial',
        explicit: {nudity: true, drugs: false},
        rating: 3,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false,
        versions: [
          {
            id: 0,
            imageLink: 'src/assets/images/123.jpg',
            imageLinkFull: 'src/assets/images/123.jpg',
            totalEarnings: '$50',
            imageType: 'illustration',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [
              {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
            ],
            headline: 'The gaping dog',
            caption: 'white background',
            keywords: [],
            location: 'Homedsadsadsadsadsaasdasd',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 3,
            date: '2015-03-6',
            rawSize: '68MB',
            resolution: '6000x3180',
            lastVersion: {
              id: 0,
              imageLink: 'src/assets/images/123.jpg',
              totalEarnings: '$50',
              imageType: 'illustration',
              categories: {
                shutterstock: [
                  'abstract',
                  'food-and-drink'
                ],
                dreamstime: [
                  'abstract--peace',
                  'nature--food-and-drink',
                  'nature--landscapes'
                ]
              },
              stocks: [
                {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
              ],
              headline: 'The gaping dog',
              caption: 'white background',
              keywords: [],
              location: 'Homedsadsadsadsadsaasdasd',
              license: 'commercial',
              explicit: {nudity: true, drugs: false},
              rating: 3,
              date: '2015-03-6',
              rawSize: '68MB',
              resolution: '6000x3180'
            }
          },
          {
            id: 1,
            imageLink: 'src/assets/images/123.jpg',
            imageLinkFull: 'src/assets/images/123.jpg',
            totalEarnings: '$50',
            imageType: 'illustration',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [
              {name: 'Adobe Stock', code: 'Adobe Stock', status: 'active'}
            ],
            headline: 'The gaping dog on white background',
            caption: 'The gaping cute dog with white hair on the white background',
            keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
            location: 'Home',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 3,
            date: '2015-03-6',
            rawSize: '68MB',
            resolution: '6000x3180',
            lastVersion: {
              id: 1,
              imageLink: 'src/assets/images/123.jpg',
              imageLinkFull: 'src/assets/images/123.jpg',
              totalEarnings: '$50',
              imageType: 'illustration',
              categories: {
                shutterstock: [
                  'abstract',
                  'food-and-drink'
                ],
                dreamstime: [
                  'abstract--peace',
                  'nature--food-and-drink',
                  'nature--landscapes'
                ]
              },
              stocks: [
                {name: 'Adobe Stock', code: 'Adobe Stock', status: 'active'}
              ],
              headline: 'The gaping dog on white background',
              caption: 'The gaping cute dog with white hair on the white background',
              keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
              location: 'Home',
              license: 'commercial',
              explicit: {nudity: true, drugs: false},
              rating: 3,
              date: '2015-03-6',
              rawSize: '68MB',
              resolution: '6000x3180'
            }
          }
        ],
        lastVersion: {
          phase: 'gallery',
          id: 0,
          imageLink: 'src/assets/images/123.jpg',
          imageLinkFull: 'src/assets/images/123.jpg',
          totalEarnings: '$50',
          imageType: 'illustration',
          categories: {
            shutterstock: [
              'abstract',
              'food-and-drink'
            ],
            dreamstime: [
              'abstract--peace',
              'nature--food-and-drink',
              'nature--landscapes'
            ]
          },
          stocks: [
            {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
          ],
          headline: 'The gaping dog on white background',
          caption: 'The gaping cute dog with white hair on the white background',
          keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
          location: 'Home',
          license: 'commercial',
          explicit: {nudity: true, drugs: false},
          date: '2015-03-6',
          rawSize: '68MB',
          resolution: '6000x3180',
          deleted: false,
          versions: [
            {
              id: 0,
              imageLink: 'src/assets/images/123.jpg',
              imageLinkFull: 'src/assets/images/123.jpg',
              totalEarnings: '$50',
              imageType: 'illustration',
              categories: {
                shutterstock: [
                  'abstract',
                  'food-and-drink'
                ],
                dreamstime: [
                  'abstract--peace',
                  'nature--food-and-drink',
                  'nature--landscapes'
                ]
              },
              stocks: [
                {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
              ],
              headline: 'The gaping dog',
              caption: 'original caption',
              keywords: [],
              location: 'Homedsadsadsadsadsaasdasd',
              license: 'commercial',
              explicit: {nudity: true, drugs: false},
              rating: 3,
              date: '2015-03-6',
              rawSize: '68MB',
              resolution: '6000x3180'
            },
            {
              phase: 'gallery',
              id: 1,
              imageLink: 'src/assets/images/123.jpg',
              imageLinkFull: 'src/assets/images/123.jpg',
              totalEarnings: '$50',
              imageType: 'illustration',
              categories: {
                shutterstock: [
                  'abstract',
                  'food-and-drink'
                ],
                dreamstime: [
                  'abstract--peace',
                  'nature--food-and-drink',
                  'nature--landscapes'
                ]
              },
              stocks: [
                {name: 'Adobe Stock', code: 'Adobe Stock', status: 'active'}
              ],
              headline: 'The gaping dog on white background',
              caption: 'The gaping cute dog with white hair on the white background',
              keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
              location: 'Home',
              license: 'commercial',
              explicit: {nudity: true, drugs: false},
              rating: 3,
              date: '2015-03-6',
              rawSize: '68MB',
              resolution: '6000x3180'
            }
          ]
        }
      },
      {
        phase: 'gallery',
        id: 1,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$50',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: 'Home',
        license: 'commercial',
        explicit: {nudity: true, drugs: false},
        rating: 1,
        date: '2013-01-1',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false,
        snapshot: {
          temperature: 0,
          tint: 0,
          exposure: 0,
          contrast: 0,
          highlights: 15,
          shadows: 0,
          blacks: 0,
          whites: 0,
          clarity: 0,
          dehaze: 0,
          vibrance: 0,
          saturation: 1,
          sharpening: {
            level: 0,
            radius: 0,
            detail: 0
          },
          noiseReduction: {
            level: 0,
            detail: 0
          },
          lensCorrection: false
        },
        lastVersion: {
          phase: 'gallery',
          id: 1,
          imageLink: 'src/assets/images/123.jpg',
          imageLinkFull: 'src/assets/images/123.jpg',
          totalEarnings: '$50',
          imageType: 'photo',
          categories: {
            shutterstock: [
              'abstract',
              'food-and-drink'
            ],
            dreamstime: [
              'abstract--peace',
              'nature--food-and-drink',
              'nature--landscapes'
            ]
          },
          stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
          headline: 'The gaping dog on white background',
          caption: 'The gaping cute dog with white hair on the white background',
          keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
          location: 'Home',
          license: 'commercial',
          explicit: {nudity: true, drugs: false},
          date: '2013-01-1',
          rawSize: '68MB',
          resolution: '6000x3180',
          deleted: false,
          snapshot: {
            temperature: 0,
            tint: 0,
            exposure: 0,
            contrast: 0,
            highlights: 15,
            shadows: 0,
            blacks: 0,
            whites: 0,
            clarity: 0,
            dehaze: 0,
            vibrance: 0,
            saturation: 1,
            sharpening: {
              level: 0,
              radius: 0,
              detail: 0
            },
            noiseReduction: {
              level: 0,
              detail: 0
            },
            lensCorrection: false
          }
        }
      }, {
        phase: 'gallery',
        id: 2,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$50',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: 'Home',
        license: 'commercial',
        explicit: {nudity: true, drugs: false},
        rating: 5,
        date: '2015-09-16',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false,
        lastVersion: {
          phase: 'gallery',
          id: 2,
          imageLink: 'src/assets/images/123.jpg',
          imageLinkFull: 'src/assets/images/123.jpg',
          totalEarnings: '$50',
          imageType: 'photo',
          categories: {
            shutterstock: [
              'abstract',
              'food-and-drink'
            ],
            dreamstime: [
              'abstract--peace',
              'nature--food-and-drink',
              'nature--landscapes'
            ]
          },
          stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
          headline: 'The gaping dog on white background',
          caption: 'The gaping cute dog with white hair on the white background',
          keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
          location: 'Home',
          license: 'commercial',
          explicit: {nudity: true, drugs: false},
          date: '2015-09-16',
          rawSize: '68MB',
          resolution: '6000x3180',
          deleted: false
        }
      }, {
        phase: 'gallery',
        id: 3,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$50',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: 'Home',
        license: 'commercial',
        explicit: {nudity: true, drugs: false},
        rating: 5,
        date: '2016-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false,
        lastVersion: {
          phase: 'gallery',
          id: 3,
          imageLink: 'src/assets/images/123.jpg',
          imageLinkFull: 'src/assets/images/123.jpg',
          totalEarnings: '$50',
          imageType: 'photo',
          categories: {
            shutterstock: [
              'abstract',
              'food-and-drink'
            ],
            dreamstime: [
              'abstract--peace',
              'nature--food-and-drink',
              'nature--landscapes'
            ]
          },
          stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
          headline: 'The gaping dog on white background',
          caption: 'The gaping cute dog with white hair on the white background',
          keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
          location: 'Home',
          license: 'commercial',
          explicit: {nudity: true, drugs: false},
          date: '2016-03-6',
          rawSize: '68MB',
          resolution: '6000x3180',
          deleted: false
        }
      }, {
        phase: 'submit',
        id: 4,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$50',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [],
        headline: 'The gaping dog dssdds dsdsds sdds',
        caption: 'The gaping dog on the white background sddssd sdds dsdssd dssd sdds dsds sdds',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'sddss', 'dsds'],
        location: 'Home',
        license: 'commercial',
        explicit: {nudity: true, drugs: false},
        rating: 0,
        date: '2015-02-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false
      }, {
        phase: 'metadata',
        id: 5,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: [],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [],
        headline: 'The gaping dog dssdds dsdsds sdds',
        caption: 'The gaping dog on the white background sddssd sdds dsdssd dssd sdds dsds sdds',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'sddss', 'dsds'],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        deleted: false
      }, {
        phase: 'review',
        id: 6,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'rejected'}],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        reasonOfRejection: {
          headline: 'Exposition',
          description: 'The photo is extremely underexposed or overexposed.'
        },
        deleted: false
      }, {
        phase: 'review',
        id: 7,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: [
            'abstract',
            'food-and-drink'
          ],
          dreamstime: [
            'abstract--peace',
            'nature--food-and-drink',
            'nature--landscapes'
          ]
        },
        stocks: [
          {name: 'Shutterstock', code: 'shutterstock', status: 'rejected'}
        ],
        headline: 'The gaping dog on white background',
        caption: 'The gaping cute dog with white hair on the white background',
        keywords: ['dog', 'gape', 'animal', 'cute', 'home', 'white', 'background', 'hair', 'pet'],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        reasonOfRejection: {
          headline: 'Out of focus',
          description: 'The photo subject is out of focus.'
        },
        deleted: false
      }, {
        phase: 'edit',
        id: 8,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: {},
          dreamstime: {}
        },
        stocks: [],
        headline: 'edited photo',
        caption: 'edited photo',
        keywords: [],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        reasonOfRejection: {
          headline: 'Out of focus',
          description: 'The photo subject is out of focus.'
        },
        deleted: false,
        snapshot: {
          temperature: -10,
          tint: 5,
          exposure: 0,
          contrast: 0,
          highlights: 0,
          shadows: 35,
          blacks: 0,
          whites: 0,
          clarity: 0,
          vibrance: 0,
          saturation: 0,
          sharpening: {
            level: 0
          },
          noiseReduction: {
            level: 0
          },
          lensCorrection: false,
          history: []
        }
      },
      {
        phase: 'gallery',
        id: 9,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: {},
          dreamstime: {}
        },
        stocks: [],
        headline: 'edited photo',
        caption: 'edited photo',
        keywords: [],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        reasonOfRejection: {
          headline: 'Out of focus',
          description: 'The photo subject is out of focus.'
        },
        deleted: false,
        snapshot: {
          temperature: -10,
          tint: 5,
          exposure: 0,
          contrast: 0,
          highlights: 0,
          shadows: 35,
          blacks: 0,
          whites: 0,
          clarity: 0,
          vibrance: 0,
          saturation: 0,
          sharpening: {
            level: 0
          },
          noiseReduction: {
            level: 0
          },
          lensCorrection: false,
          history: []
        }
      },
      {
        phase: 'edit',
        id: 10,
        imageLink: 'src/assets/images/123.jpg',
        imageLinkFull: 'src/assets/images/123.jpg',
        totalEarnings: '$0',
        imageType: 'photo',
        categories: {
          shutterstock: {},
          dreamstime: {}
        },
        stocks: [],
        headline: 'edited photo',
        caption: 'edited photo',
        keywords: [],
        location: '',
        license: 'commercial',
        explicit: {nudity: false, drugs: false},
        rating: 0,
        date: '2015-03-6',
        rawSize: '68MB',
        resolution: '6000x3180',
        reasonOfRejection: {
          headline: 'Out of focus',
          description: 'The photo subject is out of focus.'
        },
        deleted: false,
        snapshot: {
          temperature: -10,
          tint: 5,
          exposure: 0,
          contrast: 0,
          highlights: 0,
          shadows: 35,
          blacks: 0,
          whites: 0,
          clarity: 0,
          vibrance: 0,
          saturation: 0,
          sharpening: {
            level: 0
          },
          noiseReduction: {
            level: 0
          },
          lensCorrection: false,
          history: []
        }
      }
    ],
    /**
     * List of all categories to the tracked stocks.
     * */
    categories: {
      shutterstock: {
        min: 1,
        max: 1, // Todo: maybe there should be 2 categories options for these photos which has two from old times
        categories: [
          {code: 'abstract', name: 'Abstract'},
          {code: 'animals-wildlife', name: 'Animals/Wildlife'},
          {code: 'arts', name: 'The arts'},
          {code: 'background-textures', name: 'Background/Textures'},
          {code: 'beauty-fashion', name: 'Beauty/Fashion'},
          {code: 'buildings-landmarks', name: 'Buildings/Landmarks'},
          {code: 'business-finance', name: 'Business/Finance'},
          {code: 'celebrities', name: 'Celebrities'},
          {code: 'education', name: 'Education'},
          {code: 'food-and-drink', name: 'Food and Drink'},
          {code: 'healthcare-medical', name: 'Healthcare/Medical'},
          {code: 'holidays', name: 'Holidays'},
          {code: 'industrial', name: 'Industrial'},
          {code: 'interiors', name: 'Interiors'},
          {code: 'miscellaneous', name: 'Miscellaneous'},
          {code: 'nature', name: 'Nature'},
          {code: 'objects', name: 'Objects'},
          {code: 'parks-outdoor', name: 'Parks/Outdoor'},
          {code: 'people', name: 'People'},
          {code: 'religion', name: 'Religion'},
          {code: 'science', name: 'Science'},
          {code: 'signs-symbols', name: 'Signs/Symbols'},
          {code: 'sports-recreation', name: 'Sports/Recreation'},
          {code: 'technology', name: 'Technology'},
          {code: 'transportation', name: 'Transportation'},
          {code: 'vintage', name: 'Vintage'}
        ]
      },
      dreamstime: {
        min: 3,
        max: 3,
        categories: [
          {code: 'abstract--peace', name: 'Abstract -> Peace'},
          {code: 'nature--food-and-drink', name: 'Nature -> Food and Drink'},
          {code: 'animals--mammals', name: 'Animals -> Mammals'},
          {code: 'nature--landscapes', name: 'Nature -> Landscapes'}
        ]
      }
    },
    /**
     * Collections list
     * @namespace
     * @property {Array} collections
     * */
    collections: [
      {
        name: 'Abstract',
        code: 'abstract',
        photos: [
          0, 2, 3, 5
        ]
      }
    ],
    /**
     * Tracked stocks list.
     * */
    // Todo: Merge stocks and categories and add make every stock an object.
    stocks: {
      shutterstock: 'Shutterstock',
      dreamstime: 'Dreamstime',
      'adobe-stock': 'Adobe Stock'
    },
    /**
     * Here are saved which photos are selected.
     * */
    selectedPhotos: [],
    /**
     * Filters options storage.
     * */
    filters: {
      collection: null,
      rating: {},
      stocks: [],
      statuses: []
    },
    selectedVersions: [
      [], [], []
    ],
    compare: false,
    comparedVersions: []
  },
  getters: getters,
  mutations: mutations,
  actions: actions
})

export default {
  name: 'VuexStore',
  store: store
}
