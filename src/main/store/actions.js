import axios from 'axios'

export default {
  /**
   * Sets up or change rating of single photo.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - Data
   * */
  async ratePhoto ({dispatch}, data) {
    dispatch('updatePhotoMetadata', {
      index: data.id,
      change: 'rating',
      value: data.value
    })
  },

  /**
   * Sets up or change selected photos storage.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - Data
   * */
  selectPhotos ({commit}, data) {
    commit('UPDATE_SELECTED_PHOTOS', data)

    for (let photo in data.photos) {
      if (Object.prototype.hasOwnProperty.call(data.photos, photo)) {
        commit('UPDATE_SELECTED_VERSIONS', {
          photo: data.photos[photo] - 1,
          versions: []
        })
      }
    }
  },

  /**
   * Sets up or change filters storage.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - Data
   * */
  updateFilters ({commit}, data) {
    commit('UPDATE_FILTERS', data)
    commit('UPDATE_SELECTED_PHOTOS', {
      galleryName: 'gallery',
      photos: []
    })
  },

  // Fixme: version editing doesn't work.
  /**
   * Updates all selected photos with data.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} {state} - state object.
   * @param {Object} data - Data
   * */
  async updateSelectedPhotoMetadata ({dispatch, state}, data) {
    for (let selectedPhoto in state.selectedPhotos) {
      if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
        selectedPhoto = state.selectedPhotos[selectedPhoto]

        if (state.selectedVersions[selectedPhoto - 1].length > 0) {
          let selectedVersion = state.selectedVersions[selectedPhoto - 1][0]
          let versions = state.photos[selectedPhoto - 1].versions

          versions[selectedVersion - 1][data.change] = data.value

          await dispatch('updatePhotoMetadata', {
            index: selectedPhoto - 1,
            change: 'versions',
            value: versions
          })

          continue
        }

        await dispatch('updatePhotoMetadata', {
          index: selectedPhoto - 1,
          change: data.change,
          value: data.value
        })

      }
    }
  },

  async updatePhotoMetadata ({commit, state}, data) {
    if (process.env.NODE_ENV === 'production') {
      await axios.post('http://localhost:2000/update-metadata', {
        data: {
          filename: state.photos[data.index].filenameId,
          change: data.change,
          value: data.value
        }
      }).then(() => {
        commit('UPDATE_PHOTO', {
          index: data.index,
          change: data.change,
          value: data.value
        })
      }).catch((error) => {
        throw error
      })
    } else {
      commit('UPDATE_PHOTO', {
        'index': data.index,
        'change': data.change,
        'value': data.value
      })
    }
  },

  /**
   * Changes status of selected photos to submit. This is done on metadata page.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} {state} - state object.
   * */
  sendToSubmit ({dispatch, state}) {
    for (let selectedPhoto in state.selectedPhotos) {
      if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
        selectedPhoto = state.selectedPhotos[selectedPhoto]

        if (state.photos[selectedPhoto - 1].phase === 'metadata') {
          dispatch('updatePhotoMetadata', {
            'index': selectedPhoto - 1,
            'change': 'phase',
            'value': 'submit'
          })

        }

      }
    }
  },

  // Fixme: refactor...
  /**
   * Submits selected photos - changes status to gallery and add selected stocks with pending status.
   *
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} {state} - state object.
   * */
  async submitPhotos ({dispatch}) {

    dispatch('submitModule/submitPhotos')
    /*return false

     ////
     ////
     //// SUBMIT VERSION
     ////
     ////
     if (state.selectedVersions[state.selectedPhotos[0] - 1].length > 0) {
     ////
     ////
     //// /SUBMIT VERSION
     ////
     ////
     let optionIfError = 'submitGood'
     let askedOnce = false
     let selectedStocks = JSON.parse(JSON.stringify(state.submitModule.options.selectedStocks))

     let selectedPhoto = state.selectedPhotos[0]
     let selectedVersion = state.selectedVersions[state.selectedPhotos[0] - 1][0]

     // Validators:
     let metadata = {
     categories: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].categories,
     headline: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].headline,
     caption: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].caption,
     keywords: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].keywords
     }

     let validators = {
     shutterstock: new ShutterstockValidator(metadata),
     dreamstime: new DreamstimeValidator(metadata)
     }

     for (let stock in selectedStocks) {
     if (!validators[stock].validate() && !askedOnce) {
     await swal({
     title: 'Issues with submitting',
     text: 'Some images could not be sent to few stocks because there miss or have bad metadata.',
     buttons: {
     cancel: 'Cancel whole submission',
     submitGood: 'Submit photos which are ok'
     }
     }).then(function (option) {
     optionIfError = option
     askedOnce = true
     })
     }
     }

     if (optionIfError === 'submitGood') {
     let selectedPhoto = state.selectedPhotos[0]
     let selectedVersion = state.selectedVersions[state.selectedPhotos[0] - 1][0]

     // Validators:
     let metadata = {
     categories: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].categories,
     headline: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].headline,
     caption: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].caption,
     keywords: state.photos[selectedPhoto - 1].versions[selectedVersion - 1].keywords
     }

     let validators = {
     shutterstock: new ShutterstockValidator(metadata),
     dreamstime: new DreamstimeValidator(metadata)
     }
     let selectedStocksArray = []
     Object.keys(selectedStocks).map(key => {
     if (selectedStocks[key] && validators[key].validate()) {
     let object = {
     code: key,
     name: state.stocks[key],
     status: 'pending'
     }

     selectedStocksArray.push(object)
     }

     return undefined
     })

     let stocks = JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1].versions[selectedVersion - 1].stocks))
     selectedStocksArray = selectedStocksArray.map((stock) => {
     for (let stateStock in stocks) {
     if (Object.prototype.hasOwnProperty.call(stocks, stateStock)) {
     if (stocks[stateStock].code === stock.code && stocks[stateStock].status !== 'rejected') {
     let returnStock = stocks[stateStock]
     delete stocks[stateStock]

     return returnStock
     } else if (stocks[stateStock].code === stock.code && stocks[stateStock].status === 'rejected') {
     delete stocks[stateStock]

     return stock
     }
     }
     }

     return stock
     }).concat(stocks).filter((value) => value != undefined)

     let versions = JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1].versions))
     versions[selectedVersion - 1].stocks = selectedStocksArray

     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'versions',
     'value': versions
     })
     }

     let versions = JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1].versions))
     versions[selectedVersion - 1]

     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'versions',
     'value': versions
     })

     } else {
     ////
     ////
     //// /SUBMIT VERSION
     ////
     ////
     let optionIfError = 'submitGood'
     let submitMap = {shutterstock: [], dreamstime: []}
     let submitData = {shutterstock: {}, dreamstime: {}}
     let askedOnce = false
     let selectedStocks = JSON.parse(JSON.stringify(state.submitModule.options.selectedStocks))

     for (let selectedPhoto in state.selectedPhotos) {
     if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {

     selectedPhoto = state.selectedPhotos[selectedPhoto]

     // Validators:
     let metadata = {
     categories: state.photos[selectedPhoto - 1].categories,
     headline: state.photos[selectedPhoto - 1].headline,
     caption: state.photos[selectedPhoto - 1].caption,
     keywords: state.photos[selectedPhoto - 1].keywords
     }

     let validators = {
     shutterstock: new ShutterstockValidator(metadata),
     dreamstime: new DreamstimeValidator(metadata)
     }

     for (let stock in selectedStocks) {
     if (!validators[stock].validate() && !askedOnce) {
     await swal({
     title: 'Issues with submitting',
     text: 'Some images could not be sent to few stocks because there miss or have bad metadata.',
     buttons: {
     cancel: 'Cancel whole submission',
     submitGood: 'Submit photos which are ok'
     }
     }).then(function (option) {
     optionIfError = option
     askedOnce = true
     })
     }
     }
     }
     }

     if (optionIfError === 'submitGood') {
     for (let selectedPhoto in state.selectedPhotos) {
     if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
     selectedPhoto = state.selectedPhotos[selectedPhoto]

     // Validators:
     let metadata = {
     categories: state.photos[selectedPhoto - 1].categories,
     headline: state.photos[selectedPhoto - 1].headline,
     caption: state.photos[selectedPhoto - 1].caption,
     keywords: state.photos[selectedPhoto - 1].keywords
     }

     let validators = {
     shutterstock: new ShutterstockValidator(metadata),
     dreamstime: new DreamstimeValidator(metadata)
     }

     if (state.photos[selectedPhoto - 1].phase === 'submit') {
     let selectedStocksArray = []
     Object.keys(selectedStocks).map(key => {
     if (selectedStocks[key] && validators[key].validate()) {
     let object = {
     code: key,
     name: state.stocks[key],
     status: 'pending'
     }

     selectedStocksArray.push(object)
     }

     return undefined
     })

     if (selectedStocksArray.length !== 0) {
     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'phase',
     'value': 'gallery'
     })

     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'stocks',
     'value': selectedStocksArray
     })

     selectedStocksArray.map((stock) => {
     submitMap[stock.code].push(state.photos[selectedPhoto - 1].filenameId)
     submitData[stock.code][state.photos[selectedPhoto - 1].filenameId] = state.photos[selectedPhoto - 1]
     })
     }
     } else if (state.photos[selectedPhoto - 1].phase === 'gallery') {
     let selectedStocksArray = []
     Object.keys(selectedStocks).map(key => {
     if (selectedStocks[key] && validators[key].validate()) {
     let object = {
     code: key,
     name: state.stocks[key],
     status: 'pending'
     }

     selectedStocksArray.push(object)
     }

     return undefined
     })

     let stocks = JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1].stocks))
     selectedStocksArray = selectedStocksArray.map((stock) => {
     for (let stateStock in stocks) {
     if (Object.prototype.hasOwnProperty.call(stocks, stateStock)) {
     if (stocks[stateStock].code === stock.code && stocks[stateStock].status !== 'rejected') {
     let returnStock = stocks[stateStock]
     delete stocks[stateStock]

     return returnStock
     } else if (stocks[stateStock].code === stock.code && stocks[stateStock].status === 'rejected') {
     delete stocks[stateStock]

     return stock
     }
     }
     }

     return stock
     }).concat(stocks).filter((value) => value != undefined)

     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'stocks',
     'value': selectedStocksArray
     })
     }

     commit('UPDATE_PHOTO', {
     'index': selectedPhoto - 1,
     'change': 'lastVersion',
     'value': JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1]))
     })

     }
     }
      }

     axios.post('http://localhost:2000/submit', {
     map: submitMap
     }).then((response) => {
     Object.keys(submitMap).map((stock) => {
     submitMap[stock].map((filename) => {
     chrome.runtime.sendMessage(
     'abmgjhflkmhpoiaibbjlhhdckeonliee',
     {action: 'submit', stock: stock, filename: filename, data: {
     caption: submitData[stock][filename].caption,
     keywords: submitData[stock][filename].keywords,
     license: submitData[stock][filename].license,
     explicit: submitData[stock][filename].explicit,
     categories: submitData[stock][filename].categories,
     imageType: submitData[stock][filename].imageType
     }},
     (response) => {
     console.log('submitted files: ', response)
     }
     )
     })
     })
     }).catch((error) => {
     console.log('error in submitting:', error)
     })
     }*/
  },

  /**
   * Deselects all photos in selectedPhotos array.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * */
  deselectAllPhotos ({commit}) {
    commit('UPDATE_SELECTED_PHOTOS', {photos: [], galleryName: 'gallery'})
  },

  /**
   * Updates list of selected versions.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - Data
   * */
  selectPhotoVersions ({commit}, data) {
    commit('UPDATE_SELECTED_VERSIONS', data)
  },

  /**
   * Updates rating of selected version.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - Data
   * */
  ratePhotoVersion ({commit}, data) {
    commit('RATE_PHOTO_VERSION', data)
  },

  /**
   * Sets up compare view in the gallery with selected versions of the photo.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - data
   * */
  comparePhotoVersions ({commit}, data) {
    commit('COMPARE_PHOTO_VERSIONS', data)
  },

  /**
   * Cancels compare view.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * */
  cancelCompareOfPhotoVersions ({commit}) {
    commit('CANCEL_COMPARE_OF_PHOTO_VERSIONS')
  },

  // Fixme: fix problem with merging to photo itself.
  /**
   * Merges two versions of the photo.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} data - data
   * */
  mergePhotoVersions ({commit}, data) {
    commit('MERGE_PHOTO_VERSIONS', data)
  },

  /**
   * Removes selected photos.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Function} {dispatch} - Dispatch function of the Vuex store.
   * @param {Object} {state} - state object.
   * */
  async removeSelectedPhotos ({state, dispatch}) {
    for (let selectedPhoto in state.selectedPhotos) {
      if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
        selectedPhoto = state.selectedPhotos[selectedPhoto]

        if (process.env.NODE_ENV === 'production') {
          await axios.post('http://localhost:2000/remove', {
            filename: state.photos[selectedPhoto - 1].filenameId
          }).then(() => {
            dispatch('updatePhotoMetadata', {
              index: selectedPhoto - 1,
              change: 'deleted',
              value: true
            })
          }).catch((error) => {
            throw error
          })
        } else {
          await dispatch('updatePhotoMetadata', {
            index: selectedPhoto - 1,
            change: 'deleted',
            value: true
          })
        }
      }
    }

    // Do not keep deleted photos selected.
    dispatch('deselectAllPhotos')
  },

  /**
   * Removes version from version list in photo.
   * */
  removeVersion ({state, commit}) {
    if (state.selectedVersions[state.selectedPhotos[0] - 1].length > 0) {
      commit('DELETE_VERSION', {
        photo: state.selectedPhotos[0] - 1,
        version: state.selectedVersions[state.selectedPhotos[0] - 1] - 1
      })

    }
  },

  /**
   * Reverts to original state of the photo.
   */
  revert ({state, commit, dispatch}) {
    if (state.selectedVersions[state.selectedPhotos[0] - 1].length > 0) {
      dispatch('revertVersion')
    } else {

      for (let selectedPhoto in state.selectedPhotos) {
        if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
          selectedPhoto = state.selectedPhotos[selectedPhoto]
          commit('REVERT_PHOTO', {
            'index': selectedPhoto - 1
          })

        }
      }
    }
  },

  /**
   * Reverts version to its lastVersion state.
   * */
  revertVersion ({state, commit}) {
    let selectedPhoto = state.selectedPhotos[0]

    commit('REVERT_VERSION', {
      index: selectedPhoto - 1,
      version: state.selectedVersions[selectedPhoto - 1][0] - 1
    })
  },

  /**
   * Commits changes of photos into stocks.
   * @param {Function} {commit} - Commit function of the Vuex store.
   * @param {Object} {state} - state object.
   * @param {Object} {getters} - list of Vuex getters.
   */
  commitChanges ({state, commit, getters}) {
    for (let selectedPhoto in state.selectedPhotos) {
      if (Object.prototype.hasOwnProperty.call(state.selectedPhotos, selectedPhoto)) {
        selectedPhoto = state.selectedPhotos[selectedPhoto]

        if (getters.isModified(selectedPhoto)) {
          const lastVersion = JSON.parse(JSON.stringify(state.photos[selectedPhoto - 1]))
          delete lastVersion.lastVersion

          dispatch('updatePhotoMetadata', {
            'index': selectedPhoto - 1,
            'change': 'lastVersion',
            'value': lastVersion
          })

        }
      }
    }
  },

  /**
   * Commits version changes to stocks.
   * */
  commitVersionChanges ({state, getters}) {
    if (state.selectedVersions[state.selectedPhotos[0] - 1].length > 0) {
      let selectedVersion = state.selectedVersions[state.selectedPhotos[0] - 1][0]

      if (getters.isModified(state.selectedPhotos[0], selectedVersion)) {
        const photo = JSON.parse(JSON.stringify(state.photos[state.selectedPhotos[0] - 1]))

        delete photo.versions[selectedVersion - 1].lastVersion
        photo.versions[selectedVersion - 1].lastVersion = JSON.parse(JSON.stringify(
          photo.versions[selectedVersion - 1]
        ))

        dispatch('updatePhotoMetadata', {
          'index': state.selectedPhotos[0] - 1,
          'change': 'versions',
          'value': photo.versions
        })

      }
    }
  },

  /**
   * Creates a new version in selected photo.
   * */
  createNewVersion ({state, commit}) {
    let photoToDivorce = state.photos[state.selectedPhotos[0] - 1].id

    commit('CREATE_NEW_VERSION', {
      id: photoToDivorce
    })
  }
}
