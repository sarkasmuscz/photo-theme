import diff from 'deep-diff'

export default {
  /**
   * Returns all photos which belong to the phase.
   * @param state - Injected state of the Vuex store.
   * @return {Function} callback which returns photos.
   * */
  photos (state) {
    return (phase) => {
      if (phase === 'all') {
        return state.photos

      }

      let result = []

      for (let photo in state.photos) {
        if (Object.prototype.hasOwnProperty.call(state.photos, photo)) {
          let photoObject = state.photos[photo]

          if (photoObject.phase === phase) {
            result[photo] = photoObject

          }

        }
      }

      return result
    }
  },

  /**
   * Returns list of selected photos (list of IDs).
   * @param state - Injected state of the Vuex store.
   * @return {Function} Function for getting right list by galleryName.
   * */
  selectedPhotos (state) {
    return state.selectedPhotos
  },

  /**
   * Returns filter options.
   * @param state - Injected state of the Vuex store.
   * @return {Object} Filters.
   * */
  getFilters (state) {
    return JSON.parse(JSON.stringify(state.filters))
  },

  /**
   * Returns all tracked stocks.
   * @param state - Injected state of the Vuex store.
   * @return {Array} Stocks.
   * */
  getStocks (state) {
    return state.stocks
  },

  /**
   * Returns all categories for all stocks.
   * @param state - Injected state of the Vuex store.
   * @return {Object} Categories.
   * */
  getCategories (state) {
    return state.categories
  },

  /**
   * Returns list of collections (Objects).
   * @param state - Injected state of the Vuex store.
   * @return {Array} Collections.
   * */
  getCollections (state) {
    return state.collections
  },

  /**
   * Returns list of selected versions (IDs).
   * @param state - Injected state of the Vuex store.
   * @return {Array} arrays of photos with selected versions.
   * */
  selectedVersions (state) {
    return state.selectedVersions
  },

  /**
   * Returns if comparing is active.
   * */
  compare (state) {
    return state.compare
  },

  /**
   * Returns versions which are currently being compared.
   * */
  versionsToCompare (state) {
    return state.comparedVersions
  },

  /**
   * Returns function which decides if version or photo was modified (being different from lastVersion)
   * */
  isModified (state) {
    return (id, version = false) => {
      let currentPhoto

      version = isNaN(version) ? false : version

      if (version === false) {
        currentPhoto = JSON.parse(JSON.stringify(state.photos[id - 1]))

      } else {
        currentPhoto = JSON.parse(JSON.stringify(state.photos[id - 1].versions[version - 1]))
      }
      const lastVersion = currentPhoto.lastVersion

      if (lastVersion === undefined) {
        return false
      }

      delete currentPhoto.lastVersion

      try {
        delete currentPhoto.versions
        delete lastVersion.versions

        delete currentPhoto.stocks
        delete lastVersion.stocks

        delete currentPhoto.rating
        delete lastVersion.rating

        delete lastVersion.lastVersion
      } catch (e) {
        throw e
      }

      return diff(currentPhoto, lastVersion)
    }
  }
}
