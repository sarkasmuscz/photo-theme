import axios from 'axios'

export default {
  /**
   * Updates photo object in the store.
   * @param state - Injected state of the store.
   * @namespace
   * @param {Object} data - Parameters of the method.
   * @param {Number} data.index - Index of the photo in photos array in state.
   * @param {String} data.change - Attribute to change.
   * @param {*} data.value - Value of the attribute.
   * */
  UPDATE_PHOTO (state, data) {
    state.photos[data.index][data.change] = data.value
  },

  DELETE_VERSION (state, data) {
    state.photos[data.photo].versions[data.version].deleted = true
    state.selectedVersions[data.photo] = []
  },

  /**
   * Updates active filters options.
   * @param state - Injected state of the store.
   * @param {Object} data - Filter options.
   * */
  UPDATE_FILTERS (state, data) {
    state.filters.collection = data.collection
    state.filters.rating.symbol = data.rating.symbol
    state.filters.rating.value = data.rating.value
    state.filters.stocks = data.stocks
    state.filters.statuses = data.statuses
  },

  /**
   * Updates selected photos.
   * @param state - Injected state of the store.
   * @param {Object} data - Selected photos and gallery name.
   * */
  UPDATE_SELECTED_PHOTOS (state, data) {
    state.selectedPhotos = data.photos
  },

  /**
   * Updates selected versions.
   * @param state - Injected state of the store.
   * @param {Object} data - Selected versions and photo id.
   * */
  UPDATE_SELECTED_VERSIONS (state, data) {
    let versions = state.selectedVersions.slice(0)
    versions[data.photo] = data.versions

    state.selectedVersions = versions
  },

  /**
   * Rates photo version.
   * @param state - Injected state of the store.
   * @param {Object} data - Photo id, version id and value of the rating.
   * */
  RATE_PHOTO_VERSION (state, data) {
    state.photos[data.photo].versions[data.version - 1].rating = data.value
  },

  /**
   * Sets up compare state of two versions.
   * @param {Object} state - Vuex state.
   * @param {Object} data - payload.
   */
  COMPARE_PHOTO_VERSIONS (state, data) {
    state.compare = true
    state.comparedVersions = [data.primary, data.secondary]
  },

  /**
   * Cancels compare state.
   * @param {Object} state - Vuex state.
   */
  CANCEL_COMPARE_OF_PHOTO_VERSIONS (state) {
    state.compare = false
  },

  /**
   * Merges two photo versions. One into another.
   * @param {Object} state - Vuex state.
   * @param {Object} data - payload.
   */
  MERGE_PHOTO_VERSIONS (state, data) {
    for (let property in state.photos[data.photo].versions[data.secondary - 1]) {
      if (Object.prototype.hasOwnProperty.call(state.photos[data.photo].versions[data.secondary - 1], property)) {
        if (['imageLink', 'id', 'stocks'].indexOf(property) === -1) {
          state.photos[data.photo].versions[data.primary - 1][property]
            = state.photos[data.photo].versions[data.secondary - 1][property]

        }

      }

    }
  },

  // Todo: create url /revert-photo, which would fix problem with single bad request of multiple successful
  /**
   * Reverts photo to original state.
   * @param {Object} state - Vuex state.
   * @param {Object} data - payload.
   */
  REVERT_PHOTO (state, data) {
    let photo = state.photos[data.index]
    let lastVersion = JSON.parse(JSON.stringify(photo.lastVersion))

    for (let property in lastVersion) {
      if (Object.prototype.hasOwnProperty.call(lastVersion, property)) {
        if (['versions'].indexOf(property) === -1) {
          photo[property] = lastVersion[property]

          axios.post('http://localhost:2000/update-metadata', {
            data: {
              filename: photo.filenameId,
              change: property,
              value: lastVersion[property]
            }
          }).then(() => {
            photo[property] = lastVersion[property]
          }).catch((error) => {
            console.log('error reverting photo:', error)
          })

        }

      }

    }
  },

  /**
   * Reverts photo to it's lastVersion state.
   * */
  REVERT_VERSION (state, data) {
    let photo = state.photos[data.index].versions[data.version]
    let lastVersion = JSON.parse(JSON.stringify(photo.lastVersion))

    for (let property in lastVersion) {
      if (Object.prototype.hasOwnProperty.call(lastVersion, property)) {
        if (['versions'].indexOf(property) === -1) {
          photo[property] = lastVersion[property]

        }

      }

    }
  },

  /**
   * Divorces a new version from base photo.
   * */
  CREATE_NEW_VERSION (state, data) {
    // This is pointer.
    let photo = state.photos[data.id]

    if (typeof photo.versions === 'undefined') {
      photo.versions = []
    }

    let version = {
      id: photo.versions.length,
      imageLink: photo.imageLink,
      totalEarnings: photo.totalEarnings,
      imageType: photo.imageType,
      categories: JSON.parse(JSON.stringify(photo.categories)),
      stocks: [],
      headline: photo.headline,
      caption: photo.caption,
      keywords: photo.keywords,
      location: photo.location,
      license: photo.license,
      explicit: JSON.parse(JSON.stringify(photo.explicit)),
      rating: photo.rating,
      date: photo.date,
      rawSize: photo.rawSize,
      resolution: photo.resolution
    }

    photo.versions.push(version)

    axios.post('http://localhost:2000/update-metadata', {
      data: {
        filename: photo.filenameId,
        change: 'versions',
        value: photo.versions
      }
    }).catch((error) => {
      console.log('error reverting photo:', error)
    })
  }
}
