/**
 * Thrown when outside code is trying to add parameter into the method and value of the parameter has
 * invalid type and/or has not expected value.
 * @module InvalidParametersError
 * */
export default class InvalidParameterError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'InvalidParameterError'
  }
}
