/**
 * Thrown when trying to assign value of invalid type to the property.
 * @module PropertyTypeError
 * */
export default class PropertyTypeError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'PropertyTypeError'
  }
}
