/**
 * Error thrown when outside code is trying to assign into the property value which is not
 * one of accepted values.
 * @module EnumError
 * */
export default class EnumError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'EnumError'
  }
}
