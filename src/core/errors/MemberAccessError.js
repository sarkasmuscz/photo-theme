/**
 * Thrown when trying to assign/get inaccessible property.
 * @module MemberAccessError
 * */
export default class MemberAccessError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'MemberAccessError'
  }
}
