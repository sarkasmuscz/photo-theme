/**
 * Thrown by [KeywordsMessenger](KeywordsMessenger) when amount of keywords exceeded the limit.
 * @module KeywordsLimitError
 * */
export default class KeywordsLimitError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'KeywordsLimitError'
  }
}
