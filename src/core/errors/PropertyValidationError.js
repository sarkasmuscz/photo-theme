/**
 * Thrown when property validator fails - eg. value is out of expected range of numbers.
 * @module PropertyValidationError
 * */
export default class PropertyValidationError extends Error {
  /**
   * Constructor.
   * */
  constructor (...args) {
    super(...args)
    this.name = 'PropertyValidationError'
  }
}
