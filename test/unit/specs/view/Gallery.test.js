import { createLocalVue, shallow } from 'vue-test-utils'
import Vuex from 'vuex'
import Gallery from '../../../../src/view/gallery/Gallery.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

/* eslint require-jsdoc: "off" */
describe('Gallery', function () {
  let gallery
  let actions
  let getters

  beforeEach(function () {
    actions = {
      updateFilters: jest.fn(),
      updateSelectedPhotoMetadata: jest.fn(),
      ratePhoto: jest.fn(),
      selectPhotos: jest.fn()
    }

    getters = {
      allPhotos: function () {
        return [
          {
            id: 1,
            imageLink: 'src/assets/images/125.jpg',
            totalEarnings: '$50',
            imageType: 'photo',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [{name: 'Dreamstime', code: 'dreamstime', status: 'rejected'}],
            headline: 'The gaping dog',
            caption: 'The gaping dog on the white background',
            keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
            location: 'Home',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 5,
            date: '2015-01-1',
            rawSize: '68MB',
            resolution: '6000x3180'
          }, {
            id: 2,
            imageLink: 'src/assets/images/125.jpg',
            totalEarnings: '$50',
            imageType: 'photo',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
            headline: 'The gaping dog',
            caption: 'The gaping dog on the white background',
            keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
            location: 'Home',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 1,
            date: '2013-09-16',
            rawSize: '68MB',
            resolution: '6000x3180'
          }
        ]
      },
      getFilters: function (state) {
        return state.filters
      },
      selectedPhotos: function () {
        return function () {
          return [1, 2]
        }
      }
    }

    let store = new Vuex.Store({
      state: {
        filters: {
          rating: {
            symbol: 'greaterThan',
            value: 0
          },
          stocks: [],
          statuses: []
        }
      },
      actions: actions,
      getters: getters
    })

    gallery = shallow(Gallery, {
      store,
      localVue
    })
  })

  describe('Properties', function () {
    it('should have computed property photos', function () {
      expect(gallery.vm.photos).toEqual(getters.allPhotos())
    })

    it('should have computed property filteredPhotos', function () {
      expect(gallery.vm.filteredPhotos).toEqual(getters.allPhotos())
    })

    it('should have computed property allPhotos', function () {
      expect(gallery.vm.allPhotos).toEqual(getters.allPhotos())
    })
  })

  describe('FilterPhotosMixin', function () {
    it('should be able to filter by rating with symbol greaterThan', function () {
      gallery.vm.$store.state.filters = {
        rating: {
          symbol: 'greaterThan',
          value: 3
        }
      }

      expect(gallery.vm.filteredPhotos).toEqual([getters.allPhotos()[0]])
    })

    it('should be able to filter by rating with symbol lowerThan', function () {
      gallery.vm.$store.state.filters = {
        rating: {
          symbol: 'lowerThan',
          value: 3
        }
      }

      expect(gallery.vm.filteredPhotos).toEqual([getters.allPhotos()[1]])
    })

    it('should be able to filter by rating with symbol equal', function () {
      gallery.vm.$store.state.filters = {
        rating: {
          symbol: 'equal',
          value: 5
        }
      }

      expect(gallery.vm.filteredPhotos).toEqual([getters.allPhotos()[0]])
    })

    it('should be able to filter by many stocks', function () {
      gallery.vm.$store.state.filters = {
        stocks: ['dreamstime', 'shutterstock']
      }

      expect(gallery.vm.filteredPhotos).toEqual(getters.allPhotos())
    })

    it('should be able to filter by single stock', function () {
      gallery.vm.$store.state.filters = {
        stocks: ['shutterstock']
      }

      expect(gallery.vm.filteredPhotos).toEqual([getters.allPhotos()[1]])
    })

    it('should be able to filter by many statuses', function () {
      gallery.vm.$store.state.filters = {
        statuses: ['active', 'rejected']
      }

      expect(gallery.vm.filteredPhotos).toEqual(getters.allPhotos())
    })

    it('should be able to filter by single status', function () {
      gallery.vm.$store.state.filters = {
        statuses: ['active']
      }

      expect(gallery.vm.filteredPhotos).toEqual([getters.allPhotos()[1]])
    })
  })

  describe('SortLogicMixin', function () {
    it('should be able to sort by date', function () {
      gallery.vm.updateSort('by-date')

      expect(gallery.vm.allPhotos).toEqual([getters.allPhotos()[1], getters.allPhotos()[0]])
    })

    it('should be able to sort by rating', function () {
      gallery.vm.updateSort('by-ratings')

      expect(gallery.vm.allPhotos).toEqual([getters.allPhotos()[1], getters.allPhotos()[0]])
    })
  })

  it('should render correct contents (boxed view)', function () {
    expect(gallery.element).toMatchSnapshot()
  })

  it('should render correct contents (table view)', function () {
    gallery.vm.updateView('table')
    gallery.update()

    expect(gallery.element).toMatchSnapshot()
  })

  // Todo: test styles (photo size, view).
  // Todo: test calling store.
})
