import { shallow } from 'vue-test-utils'
import ClickableSelect from '../../../../src/bundles/app-bundle/component/clickable-select/ClickableSelect.vue'

/**
 * @todo Finish this test.
 * */
describe('ClickableSelect', function () {
  let clickableSelect

  beforeEach(function () {
    clickableSelect = shallow(ClickableSelect, {
      propsData: {
        name: 'Adobe Stock',
        check: false
      }
    })
  })

  describe('Properties', function () {
    it('should have property name', function () {
      let name = clickableSelect.vm.$options.props.name

      expect(name.type).toBe(String)
      expect(name.required).toBe(true)
      expect(clickableSelect.props('name', 'Adobe Stock'))
    })
  })

  it('should match snapshot', function () {
    expect(clickableSelect.element).toMatchSnapshot()
  })
})
