import { shallow } from 'vue-test-utils'
import CollectionsPanel from '../../../../../src/panels/gallery/collections-panel/CollectionsPanel.vue'

describe('CollectionPanel', function () {
  let collectionsPanel

  beforeEach(function () {
    collectionsPanel = shallow(CollectionsPanel, {
      propsData: {
        collections: [
          {
            name: 'Abstract',
            code: 'abstract',
            photos: [
              0, 2, 3, 5
            ]
          }
        ],
        filters: {}
      }
    })
  })

  describe('filterCollection($event)', function () {
    it('should emit collection select event with null param on "all" option selected', function () {
      let stub = jest.fn()
      collectionsPanel.vm.$on('collection-select', stub)

      collectionsPanel.vm.filterCollection({
        target: {
          value: 'all'
        }
      })

      expect(stub).toBeCalledWith(null)
    })

    it('should emit collection select event with collection id param when collection selected', function () {
      let stub = jest.fn()
      collectionsPanel.vm.$on('collection-select', stub)

      collectionsPanel.vm.filterCollection({
        target: {
          value: '0'
        }
      })

      expect(stub).toBeCalledWith('0')
    })
  })
})
