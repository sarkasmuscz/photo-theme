import { shallow } from 'vue-test-utils'
import GalleryPhotoTable from '../../../../../src/panels/gallery/gallery-photo-table/GalleryPhotoTable.vue'

describe('GalleryPhotoTable', function () {
  let galleryPhotoTable

  beforeEach(function () {
    galleryPhotoTable = shallow(GalleryPhotoTable, {
      propsData: {
        imageLink: 'path/to/image',
        totalEarnings: '$5',
        stocks: [
          {name: 'Shutterstock', status: 'active'}
        ],
        id: 1,
        score: 4,
        caption: 'Caption',
        keywords: 'abstract, beautiful, landscape, view',
        imageSize: 50,
        selectedPhotos: [1, 3]
      }
    })
  })

  describe('Properties', function () {
    it('should have imageLink and required', function () {
      let imageLink = galleryPhotoTable.vm.$options.props.imageLink

      expect(imageLink.type).toBe(String)
      expect(imageLink.required).toBe(true)
      expect(galleryPhotoTable.props('imageLink', 'url/to/image.jpg')).toBeTruthy()
    })

    it('should have totalEarnings and required', function () {
      let totalEarnings = galleryPhotoTable.vm.$options.props.totalEarnings

      expect(totalEarnings.type).toBe(String)
      expect(totalEarnings.required).toBe(true)
      expect(galleryPhotoTable.props('totalEarnings', '$50')).toBeTruthy()
    })

    it('should have stocks', function () {
      let stocks = galleryPhotoTable.vm.$options.props.stocks

      expect(stocks.type).toBe(Array)
      expect(stocks.required).toBe(false)
      expect(galleryPhotoTable.vm.stocks).toEqual([{name: 'Shutterstock', status: 'active'}])
    })

    it('should have id and required', function () {
      let id = galleryPhotoTable.vm.$options.props.id

      expect(id.type).toBe(Number)
      expect(id.required).toBeTruthy()
      expect(galleryPhotoTable.props('id', 1)).toBeTruthy()
    })

    it('should have score', function () {
      let score = galleryPhotoTable.vm.$options.props.score

      expect(score.type).toBe(Number)
      expect(score.required).toBe(false)
      expect(galleryPhotoTable.props('score', 5)).toBeTruthy()
      expect(score.validator && score.validator(7)).toBeFalsy()
    })

    it('should have caption', function () {
      let caption = galleryPhotoTable.vm.$options.props.caption

      expect(caption.type).toBe(String)
      expect(caption.required).toBe(false)
      expect(galleryPhotoTable.props('caption', 'Any caption')).toBeTruthy()
    })

    it('should have keywords', function () {
      let keywords = galleryPhotoTable.vm.$options.props.keywords

      expect(keywords.type).toBe(String)
      expect(keywords.required).toBe(false)
      expect(galleryPhotoTable.props('keywords', 'any, keywords')).toBeTruthy()
    })

    it('should have imageSize', function () {
      let imageSize = galleryPhotoTable.vm.$options.props.imageSize

      expect(imageSize.type).toBe(Number)
      expect(imageSize.required).toBe(false)
      expect(galleryPhotoTable.props('imageSize', 100)).toBeTruthy()
    })
  })

  it('should be able to select itself with single click', function () {
    let stub = jest.fn()
    galleryPhotoTable.vm.$on('select-photo', stub)

    jest.spyOn(galleryPhotoTable.vm, 'select')
    galleryPhotoTable.update()

    galleryPhotoTable.find('.gallery-photo-table').trigger('click')
    expect(galleryPhotoTable.vm.select).toBeCalled()
    expect(stub).toBeCalled()
  })

  // Todo add tests to select multiple photos.

  it('should be able to rate itself with stars', function () {
    let stub = jest.fn()

    galleryPhotoTable.vm.$on('rate-itself', stub)
    galleryPhotoTable.vm.myScore = 3

    expect(stub).toBeCalledWith(3, 1)
  })

  it('should render correct contents', function () {
    expect(galleryPhotoTable.element).toMatchSnapshot()
  })
})
