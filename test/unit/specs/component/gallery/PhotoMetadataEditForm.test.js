import { shallow } from 'vue-test-utils'
import PhotoMetadataEditForm from '../../../../../src/panels/gallery/photo-metadata-edit-form/PhotoMetadataEditForm.vue'

describe('PhotoMetadataEditForm.vue', function () {
  let component
  let photo
  let categories
  let stocks

  beforeEach(function () {
    photo = {
      id: 1,
      imageLink: 'src/assets/images/125.jpg',
      totalEarnings: '$50',
      imageType: 'photo',
      categories: {
        shutterstock: [
          'abstract',
          'food-and-drink'
        ],
        dreamstime: [
          'abstract--peace',
          'nature--food-and-drink',
          'nature--landscapes'
        ]
      },
      stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
      headline: 'The gaping dog',
      caption: 'The gaping dog on the white background',
      keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
      location: 'Home',
      license: 'commercial',
      explicit: {nudity: true, drugs: false},
      rating: 1,
      date: '2013-01-1',
      rawSize: '68MB',
      resolution: '6000x3180'
    }

    categories = {
      shutterstock: {
        min: 1,
        max: 2,
        categories: [
          {code: 'abstract', name: 'Abstract'},
          {code: 'food-and-drink', name: 'Food and Drink'},
          {code: 'animals', name: 'Animals'},
          {code: 'landscapes', name: 'Landscapes'}
        ]
      }
    }

    stocks = {
      shutterstock: 'Shutterstock',
      dreamstime: 'Dreamstime',
      'adobe-stock': 'Adobe Stock'
    }

    component = shallow(PhotoMetadataEditForm, {
      propsData: {
        selectedPhotos: [1, 3],
        allPhotos: [
          photo
        ],
        categories: categories,
        stocks: stocks
      }
    })
  })

  describe('Properties', function () {
    it('should get single selected photo', function () {
      component.vm.selectedPhotos = [1]
      expect(component.vm.photo).toEqual(photo)
    })

    it('should get categories', function () {
      expect(component.vm.categories).toEqual(categories)
    })

    it('should decide whether is selected only single photo', function () {
      expect(component.vm.single).toBe(false)

      component.vm.selectedPhotos = [1]
      expect(component.vm.single).toBe(true)
    })

    it('should get category (categories of single photo)', function () {
      component.vm.selectedPhotos = [1]
      expect(component.vm.category).toEqual(photo.categories)
    })
  })

  it('should render correct contents', function () {
    component.vm.selectedPhotos = [1]
    component.update()
    expect(component.element).toMatchSnapshot()
  })

  it('should render correct contents when many photos are selected', function () {
    expect(component.element).toMatchSnapshot()
  })
})

