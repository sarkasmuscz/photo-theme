import { shallow } from 'vue-test-utils'
import FilterAssignPanel from '../../../../../src/panels/gallery/filter-assign-panel/FilterAssignPanel.vue'

describe('FilterAssignPanel', function () {
  let filterAssignPanel

  beforeEach(function () {
    filterAssignPanel = shallow(FilterAssignPanel, {
      propsData: {
        selectedPhotos: [1, 3],
        allPhotos: [
          {
            id: 1,
            imageLink: 'src/assets/images/125.jpg',
            totalEarnings: '$50',
            imageType: 'photo',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
            headline: 'The gaping dog',
            caption: 'The gaping dog on the white background',
            keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
            location: 'Home',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 1,
            date: '2013-01-1',
            rawSize: '68MB',
            resolution: '6000x3180'
          }
        ]
      }
    })
  })

  describe('Properties', function () {
    it('should have computed property rawSize', function () {
      // Two photos are selected.
      expect(filterAssignPanel.vm.rawSize).toBe('-')

      filterAssignPanel.vm.selectedPhotos = [1]
      expect(filterAssignPanel.vm.rawSize).toBe('68MB')
    })

    it('should have computed property resolution', function () {
      // Two photos are selected.
      expect(filterAssignPanel.vm.resolution).toBe('-')

      filterAssignPanel.vm.selectedPhotos = [1]
      expect(filterAssignPanel.vm.resolution).toBe('6000x3180')
    })

    it('should have computed property score', function () {
      // Two photos are selected.
      expect(filterAssignPanel.vm.score).toBe(0)

      filterAssignPanel.vm.selectedPhotos = [1]
      expect(filterAssignPanel.vm.score).toBe(1)

      let stub = jest.fn()
      filterAssignPanel.vm.$on('rate', stub)

      filterAssignPanel.vm.score = 3

      expect(stub).toBeCalledWith(3)
    })
  })
})
