import { shallow } from 'vue-test-utils'
import FilterPanel from '../../../../../src/panels/gallery/filter-panel/FilterPanel.vue'

describe('FilterPanel', function () {
  let filterPanel

  beforeEach(function () {
    filterPanel = shallow(FilterPanel, {
      propsData: {
        stocks: {
          shutterstock: 'Shutterstock'
        },
        filters: {}
      }
    })
  })

  describe('Methods', function () {
    it('filterRateSymbol method', function () {
      let stub = jest.fn()
      filterPanel.vm.$on('filter', stub)

      filterPanel.vm.filterRateSymbol({
        target: {
          value: 'greaterThan'
        }
      })

      expect(stub).toBeCalled()
    })

    it('filterRate method', function () {
      let stub = jest.fn()
      filterPanel.vm.$on('filter', stub)

      filterPanel.vm.filterRate(4)

      expect(stub).toBeCalled()
    })

    it('filterStocks method', function () {
      let stub = jest.fn()
      filterPanel.vm.$on('filter', stub)

      filterPanel.vm.filterStocks('shutterstock', true)

      expect(stub).toBeCalled()
    })

    it('filterStocks method', function () {
      let stub = jest.fn()
      filterPanel.vm.$on('filter', stub)

      filterPanel.vm.filterStatus('rejected', true)

      expect(stub).toBeCalled()
    })
  })
})
