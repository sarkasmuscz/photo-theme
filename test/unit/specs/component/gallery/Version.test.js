import { shallow } from 'vue-test-utils'
import Version from '../../../../../src/panels/gallery/version/Version.vue'

describe('Version', function () {
  let version

  beforeEach(function () {
    let $refs = {
      rating: {
        selectedRating: 3
      }
    }
    $refs.rating.selectedRating = 3

    version = shallow(Version, {
      propsData: {
        imageLink: 'url/to/image.jpg',
        totalEarnings: '$50',
        id: 1,
        stocks: [
          {name: 'Shutterstock', status: 'active'}
        ],
        score: 5,
        selectedVersions: [1]
      }
    })

    version.vm.$refs = $refs
  })

  describe('Properties', function () {
    it('should have property imageLink', function () {
      let imageLinkProp = version.vm.$options.props.imageLink

      expect(version.vm.imageLink).toBe('url/to/image.jpg')
      expect(imageLinkProp.type).toBe(String)
      expect(imageLinkProp.required).toBe(true)
      expect(version.props('imageLink', 'path/to/another/image')).toBeTruthy()
    })

    it('should have property totalEarnings', function () {
      let totalEarningsProp = version.vm.$options.props.totalEarnings

      expect(version.vm.totalEarnings).toBe('$50')
      expect(totalEarningsProp.type).toBe(String)
      expect(totalEarningsProp.required).toBe(true)
      expect(version.props('totalEarnings', '$5')).toBeTruthy()
    })

    it('should have property id', function () {
      let idProp = version.vm.$options.props.id

      expect(version.vm.id).toBe(1)
      expect(idProp.type).toBe(Number)
      expect(idProp.required).toBe(true)
      expect(version.props('id', 3)).toBeTruthy()
    })

    it('should have property stocks', function () {
      let stocksProp = version.vm.$options.props.stocks

      expect(version.vm.stocks).toEqual([
        {name: 'Shutterstock', status: 'active'}
      ])
      expect(stocksProp.type).toBe(Array)
      expect(stocksProp.required).toBe(false)
      expect(version.props('stocks', [])).toBeTruthy()
    })

    it('should have property score', function () {
      let scoreProp = version.vm.$options.props.score

      expect(version.vm.score).toBe(5)
      expect(scoreProp.type).toBe(Number)
      expect(scoreProp.required).toBe(false)
      expect(scoreProp.validator && scoreProp.validator(7)).toBe(false)
      expect(version.props('score', 3)).toBeTruthy()
    })
  })

  describe('Events', function () {
    it('should be able to select itself with single click', function () {
      let stub = jest.fn()
      version.vm.$on('select-version', stub)

      jest.spyOn(version.vm, 'select')
      version.update()

      version.find('.version').trigger('click')
      expect(version.vm.select).toBeCalled()
      expect(stub).toBeCalled()
    })

    /*it('should emit showContextMenu event when right click is triggered', function () {
     let stub = jest.fn()
     version.vm.$on('showContextMenu', stub)

     jest.spyOn(version.vm, 'showContextMenu')
     version.update()

     version.find('.version').trigger('rightClick')
     expect(version.vm.showContextMenu).toBeCalled()
     expect(stub).toBeCalled()
     })*/

    it('should emit rate-itself when rated by star-rating', function () {
      let stub = jest.fn()
      version.vm.$on('rate-itself', stub)

      version.vm.myScore = 3

      expect(stub).toBeCalledWith(1, 3)
    })
  })

  describe('Design', function () {
    it('should render correct design', function () {
      expect(version.element).toMatchSnapshot()
    })
  })
})
