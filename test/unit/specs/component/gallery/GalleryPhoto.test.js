import { shallow } from 'vue-test-utils'
import GalleryPhoto from '../../../../../src/panels/gallery/gallery-photo/GalleryPhoto.vue'

describe('GalleryPhoto.vue', function () {
  let component

  beforeEach(function () {
    component = shallow(GalleryPhoto, {
      propsData: {
        imageLink: 'url/to/image.jpg',
        totalEarnings: '$50',
        id: 1,
        stocks: [
          {name: 'Shutterstock', status: 'active'}
        ],
        score: 5,
        selectedPhotos: [1, 3]
      }
    })
  })

  /**
   * Test if it is rendering well.
   * */
  it('should have computed property galleryName which should return "gallery"', function () {
    expect(component.vm.galleryName).toBe('gallery')
  })

  describe('Properties', function () {
    it('should have imageLink and required', function () {
      let imageLink = component.vm.$options.props.imageLink

      expect(imageLink.type).toBe(String)
      expect(imageLink.required).toBe(true)
      expect(component.props('imageLink', 'url/to/image.jpg')).toBeTruthy()
    })

    it('should have totalEarnings and required', function () {
      let totalEarnings = component.vm.$options.props.totalEarnings

      expect(totalEarnings.type).toBe(String)
      expect(totalEarnings.required).toBe(true)
      expect(component.props('totalEarnings', '$50')).toBeTruthy()
    })

    it('should have stocks', function () {
      let stocks = component.vm.$options.props.stocks

      expect(stocks.type).toBe(Array)
      expect(stocks.required).toBe(false)
      expect(component.vm.stocks).toEqual([{name: 'Shutterstock', status: 'active'}])
    })

    it('should have id and required', function () {
      let id = component.vm.$options.props.id

      expect(id.type).toBe(Number)
      expect(id.required).toBe(true)
      expect(component.props('id', 1)).toBeTruthy()
    })

    it('should have score', function () {
      let score = component.vm.$options.props.score

      expect(score.type).toBe(Number)
      expect(score.required).toBe(false)
      expect(component.props('score', 5)).toBeTruthy()
      expect(score.validator && score.validator(7)).toBe(false)
    })
  })

  it('should be able to select itself with single click', function () {
    let stub = jest.fn()
    component.vm.$on('select-photo', stub)

    jest.spyOn(component.vm, 'select')
    component.update()

    component.find('.gallery-photo').trigger('click')
    expect(component.vm.select).toBeCalled()
    expect(stub).toBeCalled()
  })

  // Todo add tests to select multiple photos

  it('should be able to rate itself with stars', function () {
    let stub = jest.fn()

    component.vm.$on('rate-itself', stub)
    component.vm.myScore = 3

    expect(stub).toBeCalledWith(3, 1)
  })

  it('should render correct contents', function () {
    expect(component.element).toMatchSnapshot()
  })
})
