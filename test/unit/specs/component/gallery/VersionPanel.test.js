import { shallow } from 'vue-test-utils'
import VersionPanel from '../../../../../src/panels/gallery/version-panel/VersionPanel.vue'

describe('VersionPanel', function () {
  let versionPanel
  let photo

  beforeEach(function () {
    photo = {
      id: 1,
      imageLink: 'src/assets/images/125.jpg',
      totalEarnings: '$50',
      imageType: 'photo',
      categories: {
        shutterstock: [
          'abstract',
          'food-and-drink'
        ],
        dreamstime: [
          'abstract--peace',
          'nature--food-and-drink',
          'nature--landscapes'
        ]
      },
      stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
      headline: 'The gaping dog',
      caption: 'The gaping dog on the white background',
      keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
      location: 'Home',
      license: 'commercial',
      explicit: {nudity: true, drugs: false},
      rating: 1,
      date: '2013-01-1',
      rawSize: '68MB',
      resolution: '6000x3180',
      versions: [
        {
          id: 2,
          imageLink: 'src/assets/images/125.jpg',
          totalEarnings: '$50',
          imageType: 'photo',
          categories: {
            shutterstock: [
              'abstract',
              'food-and-drink'
            ],
            dreamstime: [
              'abstract--peace',
              'nature--food-and-drink',
              'nature--landscapes'
            ]
          },
          stocks: [{name: 'Shutterstock', code: 'shutterstock', status: 'active'}],
          headline: 'The gaping dog',
          caption: 'The gaping dog on the white background',
          keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
          location: 'Home',
          license: 'commercial',
          explicit: {nudity: true, drugs: false},
          rating: 1,
          date: '2013-01-1',
          rawSize: '68MB',
          resolution: '6000x3180'
        }
      ]
    }

    versionPanel = shallow(VersionPanel, {
      propsData: {
        photo: photo,
        selectedPhotos: [1],
        selectedVersions: [1]
      }
    })
  })

  describe('Properties', function () {
    it('should have property photo', function () {
      let photoProp = versionPanel.vm.$options.props.photo

      expect(versionPanel.vm.photo).toBe(photo)
      expect(photoProp.type).toBe(Object)
      expect(photoProp.required).toBe(false)
      expect(versionPanel.props('photo', photo)).toBeTruthy()
    })

    it('should have property selectedPhotos', function () {
      let selectedPhotosProp = versionPanel.vm.$options.props.selectedPhotos

      expect(versionPanel.vm.selectedPhotos).toEqual([1])
      expect(selectedPhotosProp.type).toBe(Array)
      expect(selectedPhotosProp.required).toBe(true)
      expect(versionPanel.props('selectedPhotos', photo)).toBeTruthy()
    })

    it('should have computed property selectedVersions', function () {
      let selectedVersionsProp = versionPanel.vm.$options.props.selectedVersions

      expect(versionPanel.vm.selectedVersions).toEqual([1])
      expect(selectedVersionsProp.type).toBe(Array)
      expect(selectedVersionsProp.required).toBe(true)
    })
  })

  describe('Events', function () {
    it('should emit select event', function () {
      let stub = jest.fn()
      versionPanel.vm.$on('select', stub)

      versionPanel.vm.select(1)
      expect(stub).toBeCalledWith(1)
    })

    it('should emit merge event', function () {
      let stub = jest.fn()
      versionPanel.vm.$on('merge', stub)

      versionPanel.vm.select(3)
      versionPanel.vm.merge(1)
      expect(stub).toBeCalledWith(2, 1)
    })

    it('should emit compare event', function () {
      let stub = jest.fn()
      versionPanel.vm.$on('compare', stub)

      versionPanel.vm.select(3)
      versionPanel.vm.compare(1)
      expect(stub).toBeCalledWith(2, 1)
    })

    it('should emit edit event', function () {
      let stub = jest.fn()
      versionPanel.vm.$on('edit', stub)

      versionPanel.vm.edit(1)
      expect(stub).toBeCalledWith(1)
    })
  })

  describe('Design', function () {
    it('should render corrent content', function () {
      expect(versionPanel.element).toMatchSnapshot()
    })

    it('should render corrent content when no photo is selected', function () {
      versionPanel.vm.selectedPhotos = []
      expect(versionPanel.element).toMatchSnapshot()
    })
  })
})
