import { shallow } from 'vue-test-utils'
import SortPanel from '../../../../../src/panels/gallery/sort-panel/SortPanel.vue'

describe('SortPanel', function () {
  let sortPanel

  beforeEach(function () {
    sortPanel = shallow(SortPanel)
  })

  describe('Properties', function () {
    it('should have property size', function () {
      expect(sortPanel.vm.size).toBe(50)

      // Should be integer.
      expect(() => {
        sortPanel.vm.size = 'anything'
      }).toThrow()
    })

    it('should have property view', function () {
      expect(sortPanel.vm.view).toBe('boxed')

      // Should be either boxed or table.
      expect(() => {
        sortPanel.vm.view = 'anything'
      }).toThrow()
    })

    it('should have property sort', function () {
      expect(sortPanel.vm.sort).toBe('none')

      // Should be either none, by-date or by-ratings.
      expect(() => {
        sortPanel.vm.sort = 'anything'
      }).toThrow()
    })
  })

  describe('Events', function () {
    it('should emit an event, when size property changes', function () {
      jest.spyOn(sortPanel.vm, '$emit')
      sortPanel.update()

      sortPanel.vm.size = 70

      expect(sortPanel.vm.$emit).toBeCalledWith('sizeChange', 70)
    })

    it('should emit an event, when view property changes', function () {
      jest.spyOn(sortPanel.vm, '$emit')
      sortPanel.update()

      sortPanel.vm.view = 'table'

      expect(sortPanel.vm.$emit).toBeCalledWith('viewChange', 'table')
    })

    it('should emit an event, when sort property changes', function () {
      jest.spyOn(sortPanel.vm, '$emit')
      sortPanel.update()

      sortPanel.vm.sort = 'by-date'

      expect(sortPanel.vm.$emit).toBeCalledWith('sortChange', 'by-date')
    })
  })
})
