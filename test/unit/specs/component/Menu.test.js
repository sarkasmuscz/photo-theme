import { shallow } from 'vue-test-utils'
import CrxMenu from '../../../../src/bundles/app-bundle/component/crx-menu/CrxMenu.vue'

describe('CrxMenu.vue', function () {
  let component

  beforeEach(function () {
    component = shallow(CrxMenu)
  })

  it('should render correct contents', function () {
    expect(component.element).toMatchSnapshot()
  })
})
