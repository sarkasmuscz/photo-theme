import { createLocalVue, shallow } from 'vue-test-utils'
import Vuex from 'vuex'
import CrxLeftPanel from '../../../../src/bundles/app-bundle/component/crx-left-panel/CrxLeftPanel.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

/* eslint require-jsdoc: "off" */
describe('CrxLeftPanel', function () {
  let crxLeftPanel
  let actions
  let getters

  beforeEach(function () {
    actions = {
      updateFilters: jest.fn()
    }

    getters = {
      getFilters () {
        return {}
      },

      getStocks () {
        return [
          {
            shutterstock: 'Shutterstock',
            dreamstime: 'Dreamstime',
            'adobe-stock': 'Adobe Stock'
          }
        ]
      },

      getCollections () {
        return [
          {
            name: 'Abstract',
            code: 'abstract',
            photos: [
              0, 2, 3, 5
            ]
          }
        ]
      }
    }

    let store = new Vuex.Store({
      actions: actions,
      getters: getters
    })

    crxLeftPanel = shallow(CrxLeftPanel, {
      mocks: {
        $route: {
          name: 'gallery'
        }
      },
      store, localVue
    })
  })

  it('should have method collectionsFilter', function () {
    crxLeftPanel.vm.collectionsFilter('0')

    expect(actions.updateFilters).toBeCalled()
  })

  it('should have method filterPanelFilter', function () {
    crxLeftPanel.vm.filterPanelFilter({
      stocks: ['shutterstock']
    })

    expect(actions.updateFilters).toBeCalled()
  })

  it('should render correct contents (gallery)', function () {
    expect(crxLeftPanel.element).toMatchSnapshot()
  })

  it('should render correct contents (submit)', function () {
    crxLeftPanel.vm.$route.name = 'submit'
    crxLeftPanel.update()

    expect(crxLeftPanel.element).toMatchSnapshot()
  })
})
