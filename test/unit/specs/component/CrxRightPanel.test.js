import { createLocalVue, shallow } from 'vue-test-utils'
import Vuex from 'vuex'
import CrxRightPanel from '../../../../src/bundles/app-bundle/component/crx-right-wrapper/CrxRightWrapper.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

/* eslint require-jsdoc: "off" */
describe('CrxRightPanel', function () {
  let crxRightPanel
  let actions
  let getters

  beforeEach(function () {
    actions = {
      updateSelectedPhotoMetadata: jest.fn()
    }

    getters = {
      allPhotos () {
        return [
          {
            id: 0,
            imageLink: 'src/assets/images/125.jpg',
            totalEarnings: '$50',
            imageType: 'illustration',
            categories: {
              shutterstock: [
                'abstract',
                'food-and-drink'
              ],
              dreamstime: [
                'abstract--peace',
                'nature--food-and-drink',
                'nature--landscapes'
              ]
            },
            stocks: [
              {name: 'Dreamstime', code: 'dreamstime', status: 'active'}
            ],
            headline: 'The gaping dog',
            caption: 'The gaping dog on the white background',
            keywords: ['dog', 'gape', 'animal', 'cute', 'home'],
            location: 'Home',
            license: 'commercial',
            explicit: {nudity: true, drugs: false},
            rating: 3,
            date: '2015-03-6',
            rawSize: '68MB',
            resolution: '6000x3180'
          }
        ]
      },

      selectedPhotos () {
        return () => {
          return [0]
        }
      },

      getCategories () {
        return {
          shutterstock: {
            min: 1,
            max: 2,
            categories: [
              {code: 'abstract', name: 'Abstract'},
              {code: 'food-and-drink', name: 'Food and Drink'},
              {code: 'animals', name: 'Animals'},
              {code: 'landscapes', name: 'Landscapes'}
            ]
          },
          dreamstime: {
            min: 3,
            max: 3,
            categories: [
              {code: 'abstract--peace', name: 'Abstract -> Peace'},
              {code: 'nature--food-and-drink', name: 'Nature -> Food and Drink'},
              {code: 'animals--mammals', name: 'Animals -> Mammals'},
              {code: 'nature--landscapes', name: 'Nature -> Landscapes'}
            ]
          }
        }
      },

      getStocks () {
        return [
          {
            shutterstock: 'Shutterstock',
            dreamstime: 'Dreamstime',
            'adobe-stock': 'Adobe Stock'
          }
        ]
      }
    }

    let store = new Vuex.Store({
      actions: actions,
      getters: getters
    })

    crxRightPanel = shallow(CrxRightPanel, {
      mocks: {
        $route: {
          name: 'gallery'
        }
      },
      store, localVue
    })
  })

  it('should have method photoMetadataEditFormUpdate', function () {
    crxRightPanel.vm.photoMetadataEditFormUpdate('caption', 'New caption')

    expect(actions.updateSelectedPhotoMetadata).toBeCalled()
  })

  it('should render correct contents (gallery)', function () {
    expect(crxRightPanel.element).toMatchSnapshot()
  })

  it('should render correct contents (review)', function () {
    crxRightPanel.vm.$route.name = 'review'
    crxRightPanel.update()

    expect(crxRightPanel.element).toMatchSnapshot()
  })
})

