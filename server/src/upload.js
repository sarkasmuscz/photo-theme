import neo4j from 'neo4j-driver'
import express from 'express'
import cors from 'cors'
import fileupload from 'express-fileupload'
import uniqid from 'uniqid'
import gm from 'gm'
import bodyParser from 'body-parser'

const app = express()

let db = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', 'heslo'))

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(fileupload())

app.post('/upload', (request, response) => {
  let session = db.session()

  if (!request.files) {
    // throw error...
  }

  let photo = request.files.photo
  let id = uniqid()
  let path = `${__dirname}/storage/originals/${id}.${/(?:\.([^.]+))?$/.exec(photo.name)[1] || 'noextension'}`

  // Todo: Raw files.

  photo.mv(path, (error) => {
    if (error) {
      console.log(error)
      return response.status(500).send('move error')
    }

    let thumbnailPath = `${__dirname}/storage/thumbnails/${id}.jpg`

    gm(path)
      .resize(260, 260)
      .write(thumbnailPath, (thumbnailError) => {
        if (thumbnailError) {
          return response.status(500).send('thumbnail error')
        }

        session.run('MATCH (u:User) WHERE u.email={email} CREATE (n:Photo)-[r:BELONGS]->(u) SET n += {infos}', {
          email: 'michal@doubkovi.cz',
          infos: {
            phase: 'edit',
            filename: id,
            totalEarnings: 0,
            imageType: '',
            categories: '{}',
            stocks: '[]',
            headline: '',
            caption: '',
            keywords: [],
            location: '',
            license: 'commercial',
            explicit: JSON.stringify({nudity: false, drugs: false}),
            rating: 0,
            date: '2018-05-03',
            rawSize: '68MB',
            resolution: '6000x3180',
            snapshot: JSON.stringify({
              temperature: 0,
              tint: 0,
              exposure: 0,
              contrast: 0,
              highlights: 0,
              shadows: 0,
              blacks: 0,
              whites: 0,
              clarity: 0,
              vibrance: 0,
              saturation: 0,
              sharpening: {
                level: 0,
                radius: 0,
                detail: 0
              },
              noiseReduction: {
                level: 0,
                detail: 0
              },
              lensCorrection: false
            }),
            versions: '[]',
            lastVersion: '{}'
          }
        }).subscribe({
          onCompleted () {
            response.status(200).send('OK')
          },

          onError (dbError) {
            console.log(dbError)

            return response.status(500).send('db error')
          }
        })
      })
  })
})

app.post('/init/upload', (request, response) => {
  let session = db.session()

  if (!request.files) {
    // throw error...
  }

  let photo = request.files.photo
  let id = uniqid()
  let path = `${__dirname}/storage/originals/${id}.${/(?:\.([^.]+))?$/.exec(photo.name)[1] || 'noextension'}`

  photo.mv(path, (error) => {
    if (error) {
      // throw error.
    }

    let thumbnailPath = `${__dirname}/storage/thumbnails/${id}.jpg`

    gm(path)
      .resize(260, 260)
      .write(thumbnailPath, (thumbnailError) => {
        if (thumbnailError) {
          // throw error...
        }

        let stockImages = JSON.parse(request.body.stockImages)
        let stocks = Object.keys(stockImages).map((key) => {
          return {code: key, name: key, status: stockImages[key].status}
        })

        let headline = stockImages[Object.keys(stockImages)[0]].headline
        let caption = stockImages[Object.keys(stockImages)[0]].caption
        let keywords = stockImages[Object.keys(stockImages)[0]].keywords
        let categories = stockImages[Object.keys(stockImages)[0]].categories
        let earnings = stockImages[Object.keys(stockImages)[0]].earnings

        session.run('MATCH (u:User) WHERE u.email={email} CREATE (n:Photo)-[r:BELONGS]->(u) SET n += {infos}', {
          email: 'michal@doubkovi.cz',
          infos: {
            phase: 'gallery',
            filename: id,
            totalEarnings: earnings,
            imageType: 'photo',
            categories: JSON.stringify(categories),
            stocks: JSON.stringify(stocks),
            headline: headline,
            caption: caption,
            keywords: keywords,
            location: '',
            license: 'commercial',
            explicit: JSON.stringify({nudity: false, drugs: false}),
            rating: 0,
            date: '05-03-2018',
            rawSize: '68MB',
            resolution: '6000x3180',
            snapshot: JSON.stringify({
              temperature: 0,
              tint: 0,
              exposure: 0,
              contrast: 0,
              highlights: 0,
              shadows: 0,
              blacks: 0,
              whites: 0,
              clarity: 0,
              vibrance: 0,
              saturation: 0,
              sharpening: {
                level: 0,
                radius: 0,
                detail: 0
              },
              noiseReduction: {
                level: 0,
                detail: 0
              },
              lensCorrection: false
            }),
            versions: '[]',
            lastVersion: JSON.stringify({
              phase: 'gallery',
              filename: id,
              totalEarnings: earnings,
              imageType: 'photo',
              categories: JSON.stringify(categories),
              stocks: JSON.stringify(stocks),
              headline: headline,
              caption: caption,
              keywords: keywords,
              location: '',
              license: 'commercial',
              explicit: JSON.stringify({nudity: false, drugs: false}),
              rating: 0,
              date: '05-03-2018',
              rawSize: '68MB',
              resolution: '6000x3180',
              snapshot: JSON.stringify({
                temperature: 0,
                tint: 0,
                exposure: 0,
                contrast: 0,
                highlights: 0,
                shadows: 0,
                blacks: 0,
                whites: 0,
                clarity: 0,
                vibrance: 0,
                saturation: 0,
                sharpening: {
                  level: 0,
                  radius: 0,
                  detail: 0
                },
                noiseReduction: {
                  level: 0,
                  detail: 0
                },
                lensCorrection: false
              })
            })
          }
        }).subscribe({
          onCompleted () {
            response.status(200).send('OK')
          },

          onError (dbError) {
            // throw error

            return response.status(500).send('db error')
          }
        })
      })
  })
})

app.listen(3000)
