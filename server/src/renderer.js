import neo4j from 'neo4j-driver'
import express from 'express'
import fs from 'fs'

const app = express()

let db = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', 'heslo'))

app.use('/static', express.static(`${__dirname}/../dist/static/`))
app.use('build.js', express.static(`${__dirname}/../dist/build.js`))

app.get('/', (request, response) => {
  let session = db.session()

  let store = {
    categories: {
      shutterstock: {
        min: 1,
        max: 1,
        categories: [
          {code: 'abstract', name: 'Abstract'},
          {code: 'animals-wildlife', name: 'Animals/Wildlife'},
          {code: 'arts', name: 'The arts'},
          {code: 'background-textures', name: 'Background/Textures'},
          {code: 'beauty-fashion', name: 'Beauty/Fashion'},
          {code: 'buildings-landmarks', name: 'Buildings/Landmarks'},
          {code: 'business-finance', name: 'Business/Finance'},
          {code: 'celebrities', name: 'Celebrities'},
          {code: 'education', name: 'Education'},
          {code: 'food-and-drink', name: 'Food and Drink'},
          {code: 'healthcare-medical', name: 'Healthcare/Medical'},
          {code: 'holidays', name: 'Holidays'},
          {code: 'industrial', name: 'Industrial'},
          {code: 'interiors', name: 'Interiors'},
          {code: 'miscellaneous', name: 'Miscellaneous'},
          {code: 'nature', name: 'Nature'},
          {code: 'objects', name: 'Objects'},
          {code: 'parks-outdoor', name: 'Parks/Outdoor'},
          {code: 'people', name: 'People'},
          {code: 'religion', name: 'Religion'},
          {code: 'science', name: 'Science'},
          {code: 'signs-symbols', name: 'Signs/Symbols'},
          {code: 'sports-recreation', name: 'Sports/Recreation'},
          {code: 'technology', name: 'Technology'},
          {code: 'transportation', name: 'Transportation'},
          {code: 'vintage', name: 'Vintage'}
        ]
      },
      dreamstime: {
        min: 3,
        max: 3,
        categories: [
          {code: 'abstract-peace', name: 'Abstract -> Peace'},
          {code: 'business-industries', name: 'Business -> Industries'},
          {code: 'business-metaphors', name: 'Business -> Metaphors'},
          {code: 'nature-forests', name: 'Nature-> Forests'},
          {code: 'nature-wildlife', name: 'Nature -> Wildlife'},
          {code: 'objects-toys', name: 'Objects -> Toys'}
        ]
      }
    },
    stocks: {
      shutterstock: 'Shutterstock',
      dreamstime: 'Dreamstime',
      'adobe-stock': 'Adobe Stock'
    },
    filters: {
      collection: null,
      rating: {},
      stocks: [],
      statuses: []
    },
    selectedPhotos: [],
    selectedVersions: [
      [], [], []
    ],
    compare: false,
    comparedVersions: []
  }
  let photos = []

  session.run('MATCH (p:Photo)-[r:BELONGS]->(u:User) WHERE u.email=$email RETURN p', {email: 'michal@doubkovi.cz'})
    .then((result) => {
      result.records.map((record) => {
        let photo = record.get(0)

        let lastVersion = JSON.parse(photo.properties.lastVersion)

        console.log(photo, lastVersion.categories, typeof lastVersion.categories)

        photos.push({
          phase: photo.properties.phase,
          filenameId: photo.properties.filename,
          id: photos.length,
          imageLink: 'http://localhost:1025/image/' + photo.properties.filename + '/small',
          imageLinkFull: 'http://localhost:1025/image/' + photo.properties.filename,
          totalEarnings: photo.properties.totalEarnings,
          imageType: photo.properties.imageType,
          categories: JSON.parse(photo.properties.categories),
          stocks: JSON.parse(photo.properties.stocks),
          headline: photo.properties.headline,
          caption: photo.properties.caption,
          keywords: photo.properties.keywords,
          location: photo.properties.location,
          license: photo.properties.license,
          explicit: JSON.parse(photo.properties.explicit),
          rating: photo.properties.rating,
          date: photo.properties.date,
          rawSize: photo.properties.rawSize,
          resolution: photo.properties.resolution,
          deleted: false,
          snapshot: JSON.parse(photo.properties.snapshot || '{}'),
          versions: JSON.parse(photo.properties.versions || '[]'),
          lastVersion: photo.properties.phase === 'gallery' ? {
            phase: lastVersion.phase,
            filenameId: lastVersion.filename,
            id: photos.length,
            imageLink: 'http://localhost:1025/image/' + photo.properties.filename + '/small',
            imageLinkFull: 'http://localhost:1025/image/' + photo.properties.filename,
            totalEarnings: lastVersion.totalEarnings,
            imageType: lastVersion.imageType,
            categories: JSON.parse(lastVersion.categories),
            stocks: JSON.parse(lastVersion.stocks),
            headline: lastVersion.headline,
            caption: lastVersion.caption,
            keywords: lastVersion.keywords,
            location: lastVersion.location,
            license: lastVersion.license,
            explicit: JSON.parse(lastVersion.explicit),
            rating: lastVersion.rating,
            date: lastVersion.date,
            rawSize: lastVersion.rawSize,
            resolution: lastVersion.resolution,
            deleted: false,
            snapshot: JSON.parse(lastVersion.snapshot),
            versions: JSON.parse(lastVersion.versions || '[]')
          } : {}
        })
      })

      store.photos = photos

      fs.readFile(`${__dirname}/../dist/index.html`, 'utf8', (htmlError, html) => {
        if (htmlError) {
          // throw error
          console.log('error')
        }

        response.status(200).send(html.replace('inject::store', JSON.stringify(store)))
      })

    }).catch((error) => {
    console.log('catched', error)
  })
})

app.get('/build.js', (request, response) => {
  fs.readFile(`${__dirname}/../dist/build.js`, 'utf8', (buildError, build) => {
    if (buildError) {
      // throw error
    }

    response.status(200).send(build)
  })
})

// Todo: don't return images, which doesn't belong to user
app.get('/image/:filename', (request, response) => {
  response.sendFile(`${__dirname}/storage/originals/${request.params.filename}.jpg`)
})

app.get('/download/:filename', (request, response) => {
  response.download(`${__dirname}/storage/originals/${request.params.filename}.jpg`)
})

app.get('/download/:filename/raw', (request, response) => {
  response.download(`${__dirname}/storage/originals/${request.params.filename}.NEF`)
})

app.get('/image/:filename/small', (request, response) => {
  response.sendFile(`${__dirname}/storage/thumbnails/${request.params.filename}.jpg`)
})

app.listen(1025)
