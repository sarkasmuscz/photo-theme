import express from 'express'
import neo4j from 'neo4j-driver'
import queue from 'queue'
import gm from 'gm'
import bodyParser from 'body-parser'
import ftp from 'ftp'
import cors from 'cors'

let submitQueue = queue()
submitQueue.concurrency = 5

let db = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', 'heslo'))

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// Todo: check for illegal change properties like filename
// Todo: don't save invalid properties (which are not in type)
app.post('/update-metadata', (request, response) => {
  let session = db.session()
  let data = request.body.data
  let value = request.body.data.value

  if (['stocks', 'categories', 'snapshot', 'explicit', 'versions', 'lastVersion'].indexOf(data.change) !== -1) {
    if (data.change === 'lastVersion') {
      value = JSON.stringify({
        phase: value.phase,
        filename: value.filenameId,
        totalEarnings: value.totalEarnings,
        imageType: value.imageType,
        categories: JSON.stringify(value.categories),
        stocks: JSON.stringify(value.stocks),
        headline: value.headline,
        caption: value.caption,
        keywords: value.keywords,
        location: value.location,
        license: value.license,
        explicit: JSON.stringify(value.explicit),
        rating: value.rating,
        date: value.date,
        rawSize: value.rawSize,
        resolution: value.resolution,
        snapshot: JSON.stringify(value.snapshot)
      })
    } else if (data.change === 'versions') {
      // Todo: implement
      value = JSON.stringify(value)
    } else {
      value = JSON.stringify(value)
    }
  }

  session.run('MATCH (p:Photo)-[r:BELONGS]->(u:User) WHERE u.email={email} AND p.filename={filename} ' +
    'SET p.' + data.change + '={value}', {
    email: 'michal@doubkovi.cz',
    filename: data.filename,
    value: value
  }).then(() => {
    console.log('updated metadata', data)

    response.send('OK')
  }).catch((error) => {
    console.log('error updating metadata', error)

    response.status(500).send('Not OK')
  })
})

app.post('/submit', (request, response) => {
  let map = request.body.map
  let session = db.session()

  for (let stock in map) {
    if (Object.prototype.hasOwnProperty.call(map, stock)) {
      let filenames = map[stock]

      /*
       *
       * {
       *   shutterstock: ['54djshwe', *filename*]
       * }
       * */

      if (filenames.length > 0) {
        session.run('MATCH (s:StockCredentials)-[r:LOGINS]->(u:User) WHERE u.email={email} AND s.name={stock} RETURN s',
          {
            email: 'michal@doubkovi.cz',
            stock: stock
          }).then((results) => {
          let counter = 0
          let credentials = results.records[0].get(0)
          let client = ftp()

          client.on('error', (error) => {
            console.log('Error in FTP connection:', error)
          })

          // Fixme: Security issue: 1th provide custom ftp data, 2nd send anothers user filename and then submit.
          // stealing images from server. First check if user own this filename.
          client.on('ready', () => {
            filenames.map((filename, index) => {
              let escaped = filename.replace(/(\.|\/)/g, '')
              gm(`${__dirname}/storage/originals/${escaped}.jpg`)
                .noProfile()
                .write(`${__dirname}/storage/submit/${escaped}.jpg`, (gmError) => {
                  if (gmError) {
                    console.log('GM error: ', gmError)
                  }

                  client.put(`${__dirname}/storage/submit/${escaped}.jpg`, `${escaped}.jpg`, (ftpError) => {
                    if (ftpError) {
                      console.log('FTP error:', ftpError)
                    }

                    counter++

                    if (filenames.length >= counter) {
                      console.log('Submitted files')
                      response.send('OK')
                      client.end()
                    }
                  })
                })
            })
          })

          client.connect({
            host: credentials.properties.server,
            port: 21,
            user: credentials.properties.username,
            password: credentials.properties.password
          })
        }).catch((error) => {
          console.log('submit error: ', error)
        })
      }
    }
  }
})

// Todo: remove files in storage.
app.post('/remove', (request, response) => {
  let filename = request.body.filename
  let session = db.session()

  session.run('MATCH (p:Photo)-[r:BELONGS]->(u:User) WHERE p.filename={filename} AND u.email={email} DELETE r, p', {
    filename: filename,
    email: 'michal@doubkovi.cz'
  }).then(() => {
    response.send('OK')
  }).catch((error) => {
    console.log('error removing photo:', error)
    response.status(500).send('Not OK')
  })
})

app.listen(2000)
